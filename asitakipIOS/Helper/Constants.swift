//
//  Constants.swift
//  asitakipIOS
//
//  Created by Mac on 2.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import UIKit


let SCREEN_HEIGHT:CGFloat  = UIScreen.main.bounds.size.height
let SCREEN_WIDTH:CGFloat  = UIScreen.main.bounds.size.width

struct Constants {
    
    public static let CONNECTION_TIMEOUT : Double = 30
    static let isMyGUID:Bool = false
    static let kMyGUID = "180068D7-5209-465C-8DD9-FD1A791EDB9A"
}

struct AppConfig {
    static let baseUrl  = "http://api.markasci.com"
}

struct Service {
    static let baseURL = AppConfig.baseUrl
    
    static let Login = baseURL + "/markasciapi/vaccine/apilogin"
    static let Register = baseURL + "/markasciapi/vaccine/apiregister"
    static let Countries =  baseURL + "/markasciapi/vaccine/listcountries"
    static let Vaccines = baseURL + "/markasciapi/vaccine/listvaccines"
    static let Diseases = baseURL + "/markasciapi/vaccine/listdisease"
    static let Professions = baseURL + "/markasciapi/vaccine/listprofession"
    static let Users = baseURL + "/markasciapi/vaccine/apigetsubusers"
    static let ProcessSubUser = baseURL + "/markasciapi/vaccine/apiprocesssubuser"
    static let SubUserCalendar = baseURL + "/markasciapi/vaccine/apigetsubusercalendar"
    static let SubUserAdultCalendar = baseURL + "/markasciapi/vaccine/apigetsubuseradultcalendar"
    static let SubUserVaccineCalendar = baseURL + "/markasciapi/vaccine/apipostsubuservaccinecalendar"
    static let SubUserAdultVaccineCalendar = baseURL + "/markasciapi/vaccine/apipostsubuseradultcalendar"
    static let SubUserAdultFlow = baseURL + "/markasciapi/vaccine/apigetsubuseradultflow"
    static let SubUserAdultAnswers = baseURL + "/markasciapi/vaccine/apipostsubuseradultinitialdata"
    
}

struct ErrorCode {
    static let EMAIL_ALREADY_TAKEN:Int = 2002
    static let EMAIL_ALREADY_CONFIRMED:Int = 2110
    static let USER_NOT_FOUND:Int = 2106
    
    static func errorList() -> [Int] {
        return [ErrorCode.EMAIL_ALREADY_TAKEN,
                ErrorCode.EMAIL_ALREADY_CONFIRMED,
                ErrorCode.USER_NOT_FOUND
        ]
    }
}
