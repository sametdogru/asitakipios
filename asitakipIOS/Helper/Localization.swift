//
//  Localization.swift
//  asitakipIOS
//
//  Created by Mac on 2.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation

class Localization {

    static fileprivate func getLocalizedStringFor(_ key: String?) -> String {
        if key == nil || key == "" {
            return ""
        }
        let bundle = CoreManager.instance.bundle

        return NSLocalizedString(key!, tableName: nil, bundle: bundle, value: "", comment: "")
    }

    static func getLocalizedString(_ key: String?, args: [AnyObject]? = nil) -> String {

        let value  = Localization.getLocalizedStringFor(key)
        return value

    }
}
