//
//  CoreManager.swift
//  asitakipIOS
//
//  Created by Mac on 2.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import SwiftyJSON

open class CoreManager {
    
    let MultiLanguageSupported  = false
    
    static let instance: CoreManager = CoreManager()
    
    var language  = Locale.preferredLanguages[0]
    var bundle = Bundle.main
    
    
    func clear() {
    }
    
    var runningServiceList:[String] =  [String]()
    
    
    fileprivate init() {
        
        let langCode: String = "tr"
        let bundlePath = bundle.path(forResource: langCode, ofType: "lproj")
        
        if let path = bundlePath {
            bundle =  Bundle(path: path)!
            language = langCode + "-" + langCode.uppercased()
        }
    }
    
    public static func Instance() -> CoreManager {
        return instance
    }
    
    func logout()
    {
        KeychainService.deleteKey(userGuid)
        KeychainService.deleteKey(deviceKey)
        KeychainService.setActivationCompleted(value: false)
        runningServiceList.removeAll()
    }
}


extension CoreManager {
    
    func isRunning(urlStr:String) -> Bool
    {
        let array = runningServiceList.filter {$0.contains(urlStr)}
        if let _ =  array.first {
            log.debug("\(urlStr) is running")
            return true
        }
        log.debug("\(urlStr) is added")
        runningServiceList.append(urlStr)
        return false
    }
    
    func removeService(urlStr:String)
    {
        if let index =  runningServiceList.firstIndex(of: urlStr) {
            runningServiceList.remove(at: index)
            log.debug("\(urlStr) is removed")
        }
    }
}
