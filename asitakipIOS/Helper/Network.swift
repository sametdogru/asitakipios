//
//  Network.swift
//  asitakipIOS
//
//  Created by Mac on 2.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import SwiftyJSON

class Network
{
    static let shared = Network()
    
    func post(url path: String,
              parameters: [String: Any]? = nil,
              timeoutInterval: TimeInterval = Constants.CONNECTION_TIMEOUT,
              showIndicator: Bool = true,
              completionHandler: @escaping (_ response: JSON, MyError?) -> Swift.Void) {
    
        if CoreManager.instance.isRunning(urlStr: path) {return}
        
        var param = parameters
        if param != nil {
           
           // param!["iosDeviceId"] = KeychainService.deviceId()
            param!["lang"] = Locale.current.languageCode ?? "tr"
        }
        
        if param != nil
               {
            JSONSerialization.isValidJSONObject(_: param!)
            
                   log.debug(String(data: try! JSONSerialization.data(withJSONObject: param!, options: .prettyPrinted), encoding: .utf8)!)
               }
               
        
//        if showIndicator {
//            appDelegate.window?.showHUD()
//        }
        let request = createRequest(path: path, timeoutInterval: timeoutInterval, parameters: param)
    
        URLSession.shared.dataTask(with: request) { [weak self] (data, urlResponse, error) in
//            if showIndicator
//            {
//                appDelegate.window?.removeHUD()
//            }
            
            DispatchQueue.main.async
                { CoreManager.instance.removeService(urlStr: path)
                    
                let (json, myError) = (self?.parse(data: data, error: error, urlResponse: urlResponse))!
                    
                 completionHandler(json, myError)
            }
            }.resume()
    }
    
    func get(url path: String,
              timeoutInterval: TimeInterval = Constants.CONNECTION_TIMEOUT,
              showIndicator: Bool = true,
              completionHandler: @escaping (_ response: JSON, MyError?) -> Swift.Void) {
    
        if CoreManager.instance.isRunning(urlStr: path) {return}
        
//        if showIndicator {
//            appDelegate.window?.showHUD()
//        }
        let url = URL(string: path)!

        let request = URLRequest.init(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: timeoutInterval)
       
    
        URLSession.shared.dataTask(with: request) { [weak self] (data, urlResponse, error) in
//            if showIndicator
//            {
//                appDelegate.window?.removeHUD()
//            }
            DispatchQueue.main.async
                {
                    CoreManager.instance.removeService(urlStr: path)
                let (json, myError) = (self?.parse(data: data, error: error, urlResponse: urlResponse))!
                    
                 completionHandler(json, myError)
            }
            }.resume()
    }
    
    
    private func getFieldFromHeader(fields: [AnyHashable : Any], key: String) -> String? {
        
        if let value = fields[key] as? String {
            return !value.isEmpty ? value : nil
        } else {
            return nil
        }
    }
    
    private func header() -> [String: String] {
        
        let timestamp = String(NSDate().timeIntervalSince1970)
        
        let header =  ["ClientId": "ios",
                       "TimeStamp": timestamp]
        return header
    }
    

    private func createRequest(path: String, timeoutInterval: TimeInterval = 45, parameters: [String: Any]?) -> URLRequest
    {
        
        let url = URL(string: path)
        
        var request = URLRequest.init(url: url!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: timeoutInterval)
        request.allHTTPHeaderFields = header()
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        log.debug( "path : \(path)")
         log.debug("---------- REQUEST HEADER ----------")
        request.allHTTPHeaderFields?.forEach({ print("\($0) : \($1)") })
         log.debug("---------- REQUEST HEADER ----------")
        
        if let _ = parameters {
            
            do {
                
                 log.debug("---------- PARAMETERS ----------")
                parameters?.forEach({  print("\($0) : \($1)") })
                 log.debug("---------- PARAMETERS ----------")
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters!,
                                                              options: [])
                 log.debug((request.httpBody?.base64EncodedString())!)
            } catch let error {
                 log.error(error.localizedDescription)
            }
        }
        return request
    }
    
    
    private func parse(data: Data?, error: Error?, urlResponse: URLResponse?) -> (JSON, MyError?) {
        
        if let nsError = error as NSError? {
            return (JSON.null, MyError(error: nsError))
        }
        
        guard let data = data else{
            let error = MyError(code: "1", detail: "Json error")
            return (JSON.null, error)
        }
        
        do {
            let json =  try JSON.init(data: data)
            log.debug(json)
            if json == JSON.null {
                return (JSON.null, MyError(code: "2", detail: "Json Null"))
            } else {
                return (json, nil)
            }
        } catch  (let error){
            log.error(error.localizedDescription)
            let errorx = MyError(code: "100", detail: "JSON Format Error")
            return (JSON.null, errorx)
        }
    }
}


struct MyError: CustomDebugStringConvertible {
    let code: String
    let detail: String
    
    var debugDescription: String {
        return detail
    }
    
    init (error: NSError) {
        
        code = String(format:"%d", error.code)
        detail = error.localizedDescription
    }
    
    init(code: String, detail: String) {
        self.code = code
        self.detail = detail
    }
}
