//
//  Stackview+Extensions.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 26.10.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import UIKit

extension UIStackView {
 
    func removeFully(view: UIView) {
        removeArrangedSubview(view)
        view.removeFromSuperview()
    }

    func removeFullyAllArrangedSubviews() {
        arrangedSubviews.forEach { (view) in
            removeFully(view: view)
        }
    }
}
