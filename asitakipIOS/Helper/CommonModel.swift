//
//  CommonModel.swift
//  asitakipIOS
//
//  Created by Mac on 2.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol JSONSerializable {
    init(json: JSON)
}

protocol JSONCollectionSerializable: JSONSerializable { }

extension JSONCollectionSerializable {

    static func getCollection(json: JSON) -> [Self] {

        var list = [Self]()
        for item in json.arrayValue {
            list.append(Self(json: item))
        }
        return list
    }
}

struct CheckResponse {
    let success:Bool
    let message:String
    let code:Int
    let data:String?
    
    init(json: JSON) {
           success = json["success"].boolValue
           message = json["message"].stringValue
           code = json["code"].intValue
           data = json["data"].string
       }
}

