//
//  Arrays+Extension.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 3.11.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

//import Foundation
//
//extension Array {
//    public func toDictionary<Key: Hashable>(with selectKey: (Element) -> Key) -> [Key:Element] {
//        var dict = [Key:Element]()
//        for element in self {
//            dict[selectKey(element)] = element
//        }
//        return dict
//    }
//}
