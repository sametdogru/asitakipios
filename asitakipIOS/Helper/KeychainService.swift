//
//  KeychainService.swift
//  asitakipIOS
//
//  Created by Mac on 9.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import Security
import UIKit

// Constant Identifiers
let userAccount = "AuthenticatedUser"
let accessGroup = "SecuritySerivice"

/**
 *  User defined keys for new entry
 *  Note: add new keys for new secure item and use them in load and save methods
 */

let passwordKey = "KeyForPassword"
let deviceKey = "deviceID"
let userGuid = "userGuidx"
let pushId = "pushId"
let activation = "act"
let userActive = "UserAktif"
//Mars Card

//PFM
let pfmRemember = "pfmRemember"
// Arguments for the keychain queries
let kSecClassValue = NSString(format: kSecClass)
let kSecAttrAccountValue = NSString(format: kSecAttrAccount)
let kSecValueDataValue = NSString(format: kSecValueData)
let kSecClassGenericPasswordValue = NSString(format: kSecClassGenericPassword)
let kSecAttrServiceValue = NSString(format: kSecAttrService)
let kSecMatchLimitValue = NSString(format: kSecMatchLimit)
let kSecReturnDataValue = NSString(format: kSecReturnData)
let kSecMatchLimitOneValue = NSString(format: kSecMatchLimitOne)

open class KeychainService: NSObject {

    public static let sharedInstance = KeychainService()

    fileprivate var UID: String?

    fileprivate override init() {
        super.init()
        self.UID = KeychainService.load(passwordKey)
    }

    /**
     * Exposed methods to perform save and load queries.
     */

    open func saveUID(_ token: String) {
        KeychainService.save(passwordKey, data: token)
        self.UID = KeychainService.load(passwordKey)
    }

    open func loadUID() -> String? {
        return UID
    }

    /**
     * Internal methods for querying the keychain.
     */

    open class func getValue(_ key: String) -> String? {
        return KeychainService.load(key)
    }

    open class func setValue(_ key: String, value: String) {
        KeychainService.save(key, data: value)
    }

    open class func setUserGUID(value: String) {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: userActive)
        KeychainService.save(userGuid, data: value)
    }
    
    open class func setActivationCompleted(value: Bool) {
        KeychainService.save(activation, data: value ? "1" : "0")
    }
    
    open class func isActivationCompleted() -> Bool
    {
        guard let activation = KeychainService.getValue(activation) else { return false }
        return activation == "1"
    }
    
    open class func getUserGUID() -> String?
    {
        if Constants.isMyGUID {
            return  Constants.kMyGUID
        }
        return KeychainService.getValue(userGuid)
    }
    
    open class func getPushId() -> String?
    {
        return KeychainService.getValue(pushId)
    }
    
    open class func deleteKey(_ key: String) {
        KeychainService.delete(key)
    }
    open class func deviceId() -> String
    {
        if let deviceID =  KeychainService.load(deviceKey) {
            return deviceID
        }
        
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
             KeychainService.save(deviceKey, data: uuid)
            return uuid
        }
        return ""
    }

    fileprivate class func save(_ service: String, data: String) {
        let dataFromString: Data = data.data(using: String.Encoding.utf8, allowLossyConversion: false)!

        // Instantiate a new default keychain query
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, userAccount, dataFromString], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecValueDataValue])

        // Delete any existing items
        SecItemDelete(keychainQuery as CFDictionary)

        // Add the new keychain item
        SecItemAdd(keychainQuery as CFDictionary, nil)
    }

    fileprivate class func load(_ service: String) -> String? {
        // Instantiate a new default keychain query
        // Tell the query to return a result
        // Limit our results to one item
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, userAccount, kCFBooleanTrue, kSecMatchLimitOneValue], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecReturnDataValue, kSecMatchLimitValue])

        var dataTypeRef: AnyObject?

        // Search for the keychain items
        let status: OSStatus = SecItemCopyMatching(keychainQuery, &dataTypeRef)
        var contentsOfKeychain: String? = nil

        if status == errSecSuccess {

            if let retrievedData = dataTypeRef as? Data {
                contentsOfKeychain = String(data: retrievedData, encoding: String.Encoding.utf8)
            }
        } else {
            log.verbose("Nothing was retrieved from the keychain. Status code \(status)")
        }

        return contentsOfKeychain
    }

    fileprivate class func delete(_ service: String) {

        // Instantiate a new default keychain query
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, userAccount], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue])

        // Delete any existing items
        SecItemDelete(keychainQuery as CFDictionary)
    }
}

