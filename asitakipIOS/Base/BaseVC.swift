//
//  BaseVC.swift
//  asitakipIOS
//
//  Created by Mac on 26.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {
    
    lazy var leftNavigationItem:LeftBarItem = .none
    
    enum LeftBarItem {
        case back
        case none
        
        var imageName: String? {
            switch self {
            case .back:
                return "arrowBack"
            case .none:
                return nil
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        leftBarButton()
    }
    
    func leftBarButton() {
        switch leftNavigationItem {
        case .none:
            let button: UIButton = UIButton(type: .custom)
            button.setImage(nil, for: .normal)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        case .back:
            let barButtonItem = leftBarButtonBack(target: self, selector: #selector(leftItemAction))
            navigationItem.leftBarButtonItem = barButtonItem
        }
    }
    
    func leftBarButtonBack(target:UIViewController, selector:Selector) -> UIBarButtonItem {
        let button: UIButton = UIButton(type: .custom)
        let image = UIImage(named: "arrowBack")
        if let backImage = image {
            button.setImage(backImage, for: .normal)
            button.addTarget(target, action: selector, for: .touchUpInside)
            button.frame = CGRect(x: 0, y: 0, width: backImage.size.width, height: backImage.size.height)
            button.clipsToBounds = true
        }
        return UIBarButtonItem(customView: button)
    }
    
    @objc func leftItemAction() {
        navigationController?.popViewController(animated: true)
    }
    
     func loginVC() -> MainViewController
    {
        let vc  = MainViewController.instantiate()
        return vc
    }
    
    func alert(message: String) {
        let alert = UIAlertController(title: "Uyarı!", message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "Tamam", style: .default, handler: nil)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
           tap.cancelsTouchesInView = false
           view.addGestureRecognizer(tap)
       }
       
       @objc func dismissKeyboard() {
           view.endEditing(true)
       }
}

extension UINavigationController {

    func getViewController<T: UIViewController>(withType type: T.Type) -> T? {
        for viewController in self.viewControllers {
            if viewController is T {
                return viewController as? T
            }
        }

        return nil
    }
}
