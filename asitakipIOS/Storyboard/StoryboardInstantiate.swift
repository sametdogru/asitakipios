//
//  StoryboardInstantiate.swift
//  asitakipIOS
//
//  Created by Mac on 26.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

public protocol StoryboardInstatiate {
  static var storyboard: Storyboard { get }
  static var storybardIdentifier: String { get }
}

public extension StoryboardInstatiate where Self: UIViewController & Reusable{
  static var storybardIdentifier: String {return Self.reuseIdentifier}
  static func instantiate() -> Self {
    let bundle = Bundle(for: Self.self)
    let storyboard = UIStoryboard(name: Self.storyboard.name, bundle: bundle)
    return storyboard.instantiateViewController(withIdentifier: Self.storybardIdentifier) as! Self
  }
}

public protocol Reusable {
  static var reuseIdentifier: String {get}
}

public extension Reusable {
  static var reuseIdentifier: String {return String(describing: self)}
}

public extension Reusable where Self: UIViewController {
  init(bundle: Bundle?) {
    self.init(nibName: Self.reuseIdentifier, bundle: bundle)
  }
}

extension UIViewController {
    
    func showToast(message : String) {
        
        let fromBottom:CGFloat =  150
        
        let toastLabel = UILabel(frame: CGRect(x: 16,
                                               y: self.view.frame.size.height - fromBottom,
                                               width: self.view.frame.size.width - 32,
                                               height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.numberOfLines = 0
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
