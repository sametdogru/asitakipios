//
//  TextField.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 30.12.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import UIKit

class KeyboardAccessoryToolbar: UIToolbar {
    convenience init() {
        self.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Bitti", style: .done, target: self, action: #selector(self.done))
        self.items = [flexSpace, doneButton]
    }

    @objc func done() {
        // Tell the current first responder (the current text input) to resign.
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
