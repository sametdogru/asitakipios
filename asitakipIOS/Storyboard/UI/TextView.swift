//
//  View.swift
//  asitakipIOS
//
//  Created by Mac on 25.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

public class TextView: UIControl {
    public override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.layer.cornerRadius = 25
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.gray.cgColor
    }
}
