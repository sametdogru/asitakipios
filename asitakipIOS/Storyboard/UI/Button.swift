//
//  Button.swift
//  asitakipIOS
//
//  Created by Mac on 25.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

public class Button: UIButton {
    @IBInspectable
    public var isActive: Bool = false {
        didSet{
            if isActive {
                makeActive()
            } else {
                makePassive()
            }
        }
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        if isActive {
            makeActive()
        } else {
            makePassive()
        }
        self.layer.cornerRadius = 25
        self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        self.setTitleColor(.white, for: .normal)
    }
    
    public func makeActive() {
        isUserInteractionEnabled = true
        backgroundColor = .systemBlue
    }
    
    public func makePassive() {
        isUserInteractionEnabled = false
        backgroundColor = .lightGray
    }
}
