//
//  BarButtonItem.swift
//  asitakipIOS
//
//  Created by Mac on 26.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    class func leftButton(target:UIViewController ,selector:Selector) -> UIBarButtonItem {
        let button: UIButton = UIButton(type: .custom)
        let image = UIImage(
            named: "arrowBack")
        if let backImage = image {
            button.setImage(backImage, for: .normal)
            button.addTarget(target, action: selector, for: .touchUpInside)
            button.frame = CGRect(x: 0, y: 0, width: backImage.size.width, height: backImage.size.height)
            button.clipsToBounds = true
        }
        return UIBarButtonItem(customView: button)
    }
}
