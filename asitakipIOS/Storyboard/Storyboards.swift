//
//  Storyboards.swift
//  asitakipIOS
//
//  Created by Mac on 26.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation

public enum Storyboard: String {
  case main = "Main"
  case picker = "Picker"
  case onBoarding = "OnBoarding"
  case login = "Login"
  case register = "Register"
  case settings = "Settings"
  case trauma = "Trauma"
  case travel = "Travel"
  case vaccine = "Vaccine"
  var name: String {
    return rawValue
  }
}

