//
//  AppDelegate.swift
//  asitakipIOS
//
//  Created by Mac on 25.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit
import SwiftyBeaver

let log = SwiftyBeaver.self

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window?.makeKeyAndVisible()
        let accessoryView = KeyboardAccessoryToolbar()
            UITextField.appearance().inputAccessoryView = accessoryView
            UITextView.appearance().inputAccessoryView = accessoryView
        setupLogger()
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    

    func setupLogger() {
           
           //#if PROD
           //let logLevel = SwiftyBeaver.Level.error
           //#else
           let logLevel = SwiftyBeaver.Level.debug
           //#endif
           
           let console = ConsoleDestination()  // log to Xcode Console
           console.format = "$c$N.$F:$l - $M"
           console.minLevel = logLevel
           console.asynchronously = false
           log.addDestination(console)
       }
}

extension UIApplication {

    func applicationVersion() -> String {

        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }

    func applicationBuild() -> String {

        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }

    func versionBuild() -> String {

        let version = self.applicationVersion()
        let build = self.applicationBuild()

        log.debug("v\(version)(\(build))")
        return "v\(version)(\(build))"
    }
}

