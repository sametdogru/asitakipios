//
//  CountryService.swift
//  asitakipIOS
//
//  Created by Mac on 3.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import SwiftyJSON

//Mark: - City

struct Countries :Codable,JSONCollectionSerializable{
    let name:String
    let description:String
    
    init(json: JSON) {
        name = json["name"].stringValue
        description = json["description"].stringValue
    }
}

struct CountryService {
    
    static func getCountry(_ completionHandler: @escaping (_ cities: [Countries]?, _ error:MyError?) -> Swift.Void){
        let urlStr = String(describing: Service.Countries)
        Network.shared.get(url: urlStr) { (response, error) in
            
            if error == nil {
                print(response)
                completionHandler(Countries.getCollection(json: response["data"]),nil)
            } else {
                completionHandler(nil,error)
            }
        }
    }
}


