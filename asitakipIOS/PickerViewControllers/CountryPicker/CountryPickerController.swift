//
//  CityPickerController.swift
//  asitakipIOS
//
//  Created by Mac on 27.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

protocol CountryPickerProtocol {
    func selectedCountry(country: String)
}

protocol CountryDetailsProtocol {
    func countryDetails(description: String)
}

class CountryPickerController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var countryPickerDelegate: CountryPickerProtocol?
    var countryDetailsDelegate: CountryDetailsProtocol?
    private var countries:[Countries]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        getCities()
        tableViewConfigure()
    }
    
    func getCities() {
        CountryService.getCountry {[weak self] (cities, error) in
            self?.countries = cities
            self?.tableView.reloadData()
        }
    }
    
    func tableViewConfigure() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "CountryTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView(frame: .zero)
    }
    
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension CountryPickerController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .picker}
}

extension CountryPickerController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CountryTableViewCell
        cell.countryLabel.text = countries?[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.countryPickerDelegate?.selectedCountry(country: self.countries?[indexPath.row].name ?? "")
        self.countryDetailsDelegate?.countryDetails(description: self.countries?[indexPath.row].description ?? "")
        self.dismiss(animated: true, completion: nil)
    }
}
