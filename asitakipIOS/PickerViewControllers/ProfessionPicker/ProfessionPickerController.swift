//
//  ProfessionPickerController.swift
//  asitakipIOS
//
//  Created by Mac on 27.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

protocol ProfessionPickerProtocol {
    func selectedProfession(profession:String, index:Int)
}

class ProfessionPickerController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var delegate: ProfessionPickerProtocol?
    private var professions:[Professions]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureTableView()
        getProfessions()
    }
    
    func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ProfessionTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView(frame: .zero)
    }
    
    func getProfessions() {
        ProfessionService.getProfessions {[weak self] (professions, error) in
            self?.professions = professions
            self?.tableView.reloadData()
        }
    }
    
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ProfessionPickerController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .picker}
}

extension ProfessionPickerController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return professions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProfessionTableViewCell
        cell.professionLabel.text = professions?[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.selectedProfession(profession: professions?[indexPath.row].name ?? "", index: indexPath.row)
        self.dismiss(animated: true, completion: nil)
    }
}
