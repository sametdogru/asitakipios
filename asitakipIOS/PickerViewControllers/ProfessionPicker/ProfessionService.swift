//
//  ProfessionService.swift
//  asitakipIOS
//
//  Created by Mac on 3.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import SwiftyJSON

//Mark: - Profession

struct Professions :Codable,JSONCollectionSerializable{
    let id: String?
    let name:String?
    
    init(json: JSON) {
        id = json["id"].stringValue
        name = json["name"].stringValue
    }
}

struct ProfessionService {
    
    static func getProfessions(_ completionHandler: @escaping (_ cities: [Professions]?, _ error:MyError?) -> Swift.Void){
        let urlStr = String(describing: Service.Professions)
        Network.shared.get(url: urlStr) { (response, error) in
            
            if error == nil {
                completionHandler(Professions.getCollection(json: response["data"]),nil)
            } else {
                completionHandler(nil,error)
            }
        }
    }
}
