//
//  DatePickerController.swift
//  asitakipIOS
//
//  Created by Mac on 27.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

protocol DatePickerProtocol {
    func selectedDate(date: String)
}

class DatePickerController: UIViewController {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    var dateDesc = String()
    public var didDateSelectCompleted: ((String) -> Void)?
    var delegate: DatePickerProtocol?
    
    var isPerson: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isPerson {
            datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -100, to: Date())
        } else {
            datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -17, to: Date())
        }
        datePicker.maximumDate = Date()
        }
    
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func okButton(_ sender: Any) {
        self.dismiss(animated: true) {
            if self.dateDesc == "" {
                let currentDateTime = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "dd.MM.yyyy"
                let time = formatter.string(from: currentDateTime)
                self.delegate?.selectedDate(date: time)
                self.didDateSelectCompleted?(time)
            } else {
                self.didDateSelectCompleted?(self.dateDesc)
               self.delegate?.selectedDate(date: self.dateDesc )
            }
        }
    }

    @IBAction func setDate(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "dd.MM.yyyy"
        dateDesc = dateFormatter.string(from: datePicker.date)
        print(dateDesc)
        
//        "dd MM yyyy"
    }
}

extension DatePickerController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { .picker }
}
