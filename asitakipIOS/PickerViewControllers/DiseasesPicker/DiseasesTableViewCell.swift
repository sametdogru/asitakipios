//
//  SicknessTableViewCell.swift
//  asitakipIOS
//
//  Created by Mac on 27.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class DiseasesTableViewCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var diseaseLabel: UILabel!
    var isChecked: Bool = false
    
    func configure(isChecked: Bool) {
        self.isChecked = isChecked
        if self.isChecked {
            self.img.image = #imageLiteral(resourceName: "iconCheckbox")
        } else {
            self.img.image = #imageLiteral(resourceName: "iconUnCheckbox")
        }
    }
    
    func swapCheck(){
        isChecked = !isChecked
        configure(isChecked: isChecked)
    }
}
