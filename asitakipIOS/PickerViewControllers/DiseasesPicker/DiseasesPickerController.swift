//
//  SicknessPickerController.swift
//  asitakipIOS
//
//  Created by Mac on 27.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

protocol DiseasesPickerProtocol {
    func selectedDiseases(diseases: [String], index: [Int])
}

class DiseasesPickerController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var delegate: DiseasesPickerProtocol?
    
    private var diseases: [Diseases]?
    var chosenDiseasesIndex = [Int]()
    var chosenDiseases = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewConfigure()
        getDiseases()
    }
    
    func tableViewConfigure() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        tableView.register(UINib(nibName: "DiseasesTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }
    
    func getDiseases() {
        DiseaseService.getDiseases {[weak self] (diseases, error) in
            self?.diseases = diseases
            self?.tableView.reloadData()
        }
    }
    
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func okButton(_ sender: Any) {
        if chosenDiseasesIndex.count > 0 {
            chosenDiseasesIndex.sort()
            self.delegate?.selectedDiseases(diseases: chosenDiseases, index: chosenDiseasesIndex)
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension DiseasesPickerController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .picker}
}

extension DiseasesPickerController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return diseases?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DiseasesTableViewCell
        cell.diseaseLabel.text = diseases?[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! DiseasesTableViewCell
        cell.swapCheck()
        if cell.isChecked {
            print("add")
            self.chosenDiseasesIndex.append(indexPath.row)
            self.chosenDiseases.append("\(diseases?[indexPath.row].name ?? "")")

        } else {
            print("delete")
            if chosenDiseasesIndex.contains(indexPath.row) {
                if let index = chosenDiseasesIndex.firstIndex(of: indexPath.row) {
                    chosenDiseasesIndex.remove(at: index)
                }
            }
            
            if chosenDiseases.contains("\(diseases?[indexPath.row].name ?? "")") {
                if let index = chosenDiseases.firstIndex(of: "\(diseases?[indexPath.row].name ?? "")") {
                    chosenDiseases.remove(at: index)
//                    print(chosenDiseases)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
