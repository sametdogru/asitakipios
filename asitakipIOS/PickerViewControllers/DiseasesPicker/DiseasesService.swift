//
//  DiseasesService.swift
//  asitakipIOS
//
//  Created by Mac on 3.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import SwiftyJSON

//Mark: - Disease

struct Diseases :Codable,JSONCollectionSerializable{
    let id: String?
    let name:String?
    
    init(json: JSON) {
        id = json["id"].stringValue
        name = json["name"].stringValue
    }
}

struct DiseaseService {
    
    static func getDiseases(_ completionHandler: @escaping (_ cities: [Diseases]?, _ error:MyError?) -> Swift.Void){
        let urlStr = String(describing: Service.Diseases)
        Network.shared.get(url: urlStr) { (response, error) in
            
            if error == nil {
                completionHandler(Diseases.getCollection(json: response["data"]),nil)
            } else {
                completionHandler(nil,error)
            }
        }
    }
}
