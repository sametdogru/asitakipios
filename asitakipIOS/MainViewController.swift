//
//  MainViewController.swift
//  asitakipIOS
//
//  Created by Mac on 3.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var loginButtom: UIButton!
    @IBOutlet weak var registerButton: Button!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        descriptionLbl.textColor = .white
    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        navigationController?.isNavigationBarHidden = false
//    }

    
    @IBAction func loginButton(_ sender: Any) {
        let vc = LoginViewController.instantiate()
        self.show(vc, sender: nil)
    }
    
    @IBAction func registerButton(_ sender: Any) {
        let vc = FirstRegisterViewController.instantiate()
        self.show(vc, sender: nil)
//        if let url = URL(string: "https://www.twitter.com") {
//            let config = SFSafariViewController.Configuration()
//            config.entersReaderIfAvailable = false
//            let vc = SFSafariViewController(url: url, configuration: config)
//            present(vc, animated: true, completion: nil)
//        }
    }
}

extension MainViewController: StoryboardInstatiate,Reusable {
    public static var storyboard: Storyboard { .login }
}

