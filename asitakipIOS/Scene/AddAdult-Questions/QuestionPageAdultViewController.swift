//
//  QuestionPageAdultViewController.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 2.11.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class QuestionPageAdultViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var subUserGuid = String()
    
    var adultQuestionsArr: [AdultQuestions]?
    var answerDataDetails: AdultAnswers = AdultAnswers()
    var currentPosition = 0
    
    var vaccineDateFormat = String()
    var buttonValue = false
    var customNoteText = String()
    
    var isRegister: Bool = false
    public var didDateSelectCompleted: ((String) -> Void)?
    
    private lazy var backButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: #imageLiteral(resourceName: "arrowBack"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getQuestions()
        title = "Yetişkin Ekle"
        tableViewConfigure()
        navigationItem.leftBarButtonItem = backButton
    }
    
    func tableViewConfigure() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "QuestionPageAdultTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }
    
    func getQuestions() {
        let param = ["userGuid": KeychainService.getUserGUID() ?? "",
                     "subUserGuid": self.subUserGuid] as [String : Any]
        
        SubUserAdultQuestionService.subUserAdultQuestions(param, vc: nil) { (model, error) in
            self.adultQuestionsArr = model
            self.vaccineDateformatter(date: self.adultQuestionsArr?[self.currentPosition].suggestionDate ?? "")
            self.tableView.reloadData()
        }
    }
    
    public func vaccineDateformatter(date:String)  {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let vaccinedate = dateFormatter.date(from: date)
        let vaccineDateFormatter = DateFormatter()
        vaccineDateFormatter.dateFormat = "dd.MM.yyyy"
        vaccineDateFormat = vaccineDateFormatter.string(from: vaccinedate!)
    }
    
    @IBAction func nextButton(_ sender: Any) {
        addDataToDict()
        
        if answerDataDetails.data[currentPosition].isSelected == false {
            self.alert(message: "Evet veya Hayır seçeneklerinden birini seçmelisiniz.")
            answerDataDetails.data.remove(at: currentPosition)
        } else {
            if currentPosition < (adultQuestionsArr?.count ?? 0) - 1 {
                self.currentPosition = self.currentPosition + 1
                self.vaccineDateformatter(date: self.adultQuestionsArr?[self.currentPosition].suggestionDate ?? "")
                print(currentPosition)
                tableView.reloadData()
                self.buttonValue = false
            } else {
                answerDataDetails.userGuid = KeychainService.getUserGUID()
                answerDataDetails.subUserGuid = self.subUserGuid
                request()
            }
        }
    }
    
    private func addDataToDict() {
        print(vaccineDateFormat)
        
        let str = self.vaccineDateFormat
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        let vaccinedate = dateFormatter.date(from: str)
        
        let vaccineDateFormatter = DateFormatter()
        vaccineDateFormatter.dateFormat = "yyyy-MM-dd"
        
        let finalDateString = vaccineDateFormatter.string(from: vaccinedate!)
        print(finalDateString)
        
        answerDataDetails.data.append(AnswerDataDetails(vaccineId: self.adultQuestionsArr?[self.currentPosition].vaccineId, customVaccineDate: finalDateString, customNote: self.customNoteText, isSelected: self.buttonValue))
        
    }
    
    @objc private func back() {
        if self.currentPosition != 0 {
            self.currentPosition = self.currentPosition - 1
            tableView.reloadData()
        } else {
            if self.isRegister {
                self.navigationController?.popViewController(animated: true)
            } else {
                let vc = HomeViewController.instantiate()
                self.show(vc, sender: nil)
            }
        }
    }
    
    func request() {
        SubUserAdultAnswerService.adultAnswer(answerDataDetails.toDictionary() , vc: nil) {(checkResp, error) in
            if checkResp != nil
            {
                if !checkResp!.success {
                    print("Error")
                    self.alert(message: checkResp!.message)
                }else{
                    print(checkResp!.message)
                    print("Success")
                    
                    if self.isRegister {
                        let vc = HomeViewController.instantiate()
                        vc.isRegister = true
                        self.show(vc, sender: nil)
                    } else {
                        let vc = HomeViewController.instantiate()
                        self.show(vc, sender: nil)
                    }
                }
            }
        }
    }
}

extension QuestionPageAdultViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! QuestionPageAdultTableViewCell
        cell.delegate = self
        cell.titleLbl.text = adultQuestionsArr?[currentPosition].name
        cell.subTitleLbl.text = adultQuestionsArr?[currentPosition].detail
        cell.vaccineDateTxt.text = vaccineDateFormat
        cell.configure()
        return cell
    }
}

extension QuestionPageAdultViewController: QuestionPageAdultTableViewCellProtocol {
   
    func selectedDate(completion: @escaping ((String) -> Void)) {
        print(vaccineDateFormat)
        let vc = DatePickerController.instantiate()
        vc.isPerson = true
        vc.didDateSelectCompleted = { [unowned self] date in
            completion(date)
            self.vaccineDateFormat = date
            print(date)
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    func customNoteText(txt: String) {
        self.customNoteText = txt
        print(txt)
    }
    
    func yesButton(value: Bool) {
        self.buttonValue = value
        print(buttonValue)
    }
    
    func noButton(value: Bool, completion: @escaping ((String) -> Void)) {
        self.buttonValue = value
        self.vaccineDateformatter(date: self.adultQuestionsArr?[self.currentPosition].suggestionDate ?? "")
        completion(vaccineDateFormat)
        print(buttonValue)
    }
}

//extension QuestionPageAdultViewController: DatePickerProtocol {
//    func selectedDate(date: String) {
//        formatter(date: date)
//        vaccineDateFormat = date
//        print(vaccineDateFormat)
//        tableView.reloadData()
//    }
//
//    func formatter(date:String) {
//        let dateFormatter = DateFormatter()
//        dateFormatter.locale = .init(identifier: "tr_TR")
//        dateFormatter.dateFormat = "dd.MM.yyyy"
//
//        if let vaccinedate = dateFormatter.date(from: date) {
//            let vaccineDateFormatter = DateFormatter()
//            vaccineDateFormatter.dateFormat = "yyyy-MM-dd"
//            self.vaccineDateFormat = vaccineDateFormatter.string(from: vaccinedate)
//        }
//    }
//}

extension QuestionPageAdultViewController {
    func alert(message: String) {
        let alert = UIAlertController(title: "Uyarı!", message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "Tamam", style: .default, handler: nil)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
}

extension QuestionPageAdultViewController: StoryboardInstatiate,Reusable {
    public static var storyboard: Storyboard { return .register }
}
