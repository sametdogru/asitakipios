//
//  UserModel.swift
//  asitakipIOS
//
//  Created by Mac on 4.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//


import Foundation
import SwiftyJSON

class AnswerDataDetails: NSObject {
    var vaccineId:String?
    var customVaccineDate: String?
    var customNote:String?
    var isSelected: Bool?
    
    init(vaccineId: String?, customVaccineDate: String?, customNote: String?, isSelected: Bool?) {
        self.vaccineId = vaccineId
        self.customNote = customNote
        self.customVaccineDate = customVaccineDate
        self.isSelected = isSelected
    }
}

extension AnswerDataDetails {
   
    func toDictionary() -> [String:Any]
    {
        
        var dictionary = [String:Any]()
        
        if vaccineId != nil{
            dictionary["vaccineId"] = vaccineId
        }
        if customVaccineDate != nil{
            dictionary["customVaccineDate"] = customVaccineDate
        }
        
        if customNote != nil{
            dictionary["customNote"] = customNote
        }
        
        return dictionary
    }
}

class AdultAnswers:NSObject {
    var userGuid:String?
    var subUserGuid:String?
    var data: [AnswerDataDetails] = []
}

extension AdultAnswers {
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if userGuid != nil{
            dictionary["userGuid"] = userGuid
        }
        if subUserGuid != nil{
            dictionary["subUserGuid"] = subUserGuid
        }
        
        var dictionaryElements = [[String:Any]]()
        for element in data {
            dictionaryElements.append(element.toDictionary())
        }
        dictionary["data"] = dictionaryElements
        return dictionary
    }
}

//Mark: - Adult Questions

struct AdultQuestions :Codable,JSONCollectionSerializable {
    let suggestionDate: String?
    let vaccineId:String?
    let name:String?
    let detail:String?
    
    init(json: JSON) {
        suggestionDate = json["suggestionDate"].stringValue
        vaccineId = json["vaccineId"].stringValue
        name = json["name"].stringValue
        detail = json["detail"].stringValue
    }
}
