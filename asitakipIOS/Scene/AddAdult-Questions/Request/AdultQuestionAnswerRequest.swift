//
//  AdultQuestionAnswerRequest.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 28.10.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import SwiftyJSON

struct SubUserAdultQuestionService {
    
    static func subUserAdultQuestions(_ parameter:[String:Any],vc:BaseVC? = nil,completionHandler: @escaping ( _ response:[AdultQuestions]?, _ error:MyError?) -> Swift.Void){
        let urlStr = String(format: Service.SubUserAdultFlow, parameter)
        Network.shared.post(url: urlStr,parameters: parameter, showIndicator:true) {  (response,error) in
            print(response)
            
            if error == nil
            {
                completionHandler(AdultQuestions.getCollection(json: response["data"]),nil)
            } else {
                completionHandler(nil,error)
            }
        }
    }
}

struct SubUserAdultAnswerService {
    
    static func adultAnswer(_ parameter:[String:Any],vc:BaseVC? = nil,completionHandler: @escaping ( _ respone:CheckResponse?, _ error:MyError?) -> Swift.Void){
           let urlStr = String(format: Service.SubUserAdultAnswers, parameter)
           Network.shared.post(url: urlStr,parameters: parameter, showIndicator:true) {  (response,error) in
               
               var checkRes:CheckResponse?
                
               if error == nil
               {
               checkRes = CheckResponse(json: response)
               }
           
            if !SubUserAdultAnswerService.showError(vc, checkRes, error) {
                    completionHandler(checkRes,error)
               }
           }
       }
    
    static func showError(_ vc:BaseVC?, _ res:CheckResponse?, _ err:MyError?) -> Bool {
           
          guard let vc = vc else { return false }
       
           if let error = err {
            print(error.detail)
            print("error")
               return true
           }
           
           let errors = ErrorCode.errorList()
           
           if let response = res, !response.success
           {
               let arr = errors.filter{$0 == response.code}
               if let _ = arr.first {
                print(response.message)
                let alert = UIAlertController(title: "Uyarı!", message: response.message, preferredStyle: .alert)
                                   let okButton = UIAlertAction(title: "Tamam", style: .cancel, handler: nil)
                                   alert.addAction(okButton)
                vc.present(alert, animated: true, completion: nil)
                   return true
               }
           }
           return false
       }
}


