//
//  QuestionPageAdultTableViewCell.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 2.11.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

protocol QuestionPageAdultTableViewCellProtocol {
    func selectedDate(completion: @escaping ((String) -> Void))
    func yesButton(value: Bool)
    func noButton(value: Bool, completion: @escaping ((String) -> Void))
    func customNoteText(txt: String)
}

class QuestionPageAdultTableViewCell: UITableViewCell, UITextViewDelegate {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var vaccineDateTxt: UITextField!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var customNoteTxt: UITextView!
    @IBOutlet weak var rightArrow: UIImageView!
    
    var vaccineDateFormat = String()
    var delegate: QuestionPageAdultTableViewCellProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        noButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
        yesButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
        vaccineDateTxt.isEnabled = false
        vaccineDateTxt.textColor = .systemGray
        dateLbl.textColor = .systemGray
        rightArrow.tintColor = .systemGray
        customNoteTxt.delegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func configure() {
        noButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
        yesButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
        vaccineDateTxt.isEnabled = false
        vaccineDateTxt.textColor = .systemGray
        dateLbl.textColor = .systemGray
        rightArrow.tintColor = .systemGray
        customNoteTxt.text = ""
    }
    
    @IBAction func selectDate(_ sender: UITextField) {
        sender.resignFirstResponder()
        self.delegate?.selectedDate(completion: { date in
            sender.text = date
        })
    }
    
    @IBAction func yesButton(_ sender: UIButton) {
        if yesButton.isSelected == false {
            self.delegate?.yesButton(value: true)
            yesButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            noButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
            vaccineDateTxt.isEnabled = true
            vaccineDateTxt.textColor = .black
            dateLbl.textColor = .black
            rightArrow.tintColor = .systemBlue
        }
    }
    
    @IBAction func noButton(_ sender: UIButton) {
        if noButton.isSelected == false {
            self.delegate?.noButton(value: true, completion: { date in
                self.vaccineDateTxt.text = date
            })
            noButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            yesButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
            vaccineDateTxt.isEnabled = false
            vaccineDateTxt.textColor = .systemGray
            dateLbl.textColor = .systemGray
            rightArrow.tintColor = .systemGray
        }
    }

    func textViewDidChange(_ textView: UITextView) {
        self.delegate?.customNoteText(txt: textView.text)
    }
}
