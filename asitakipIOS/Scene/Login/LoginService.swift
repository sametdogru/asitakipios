//
//  LoginService.swift
//  asitakipIOS
//
//  Created by Mac on 9.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation

class LoginService {
    
    //MARK: Login
    static func login(_ parameter:[String:String],vc:BaseVC? = nil,completionHandler: @escaping ( _ respone:CheckResponse?, _ error:MyError?) -> Swift.Void){
        let urlStr = String(format: Service.Login, parameter)
        Network.shared.post(url: urlStr,parameters: parameter, showIndicator:true) {  (response,error) in
            
            var checkRes:CheckResponse?
            if error == nil
            {
                checkRes = CheckResponse(json: response)
                if let id = checkRes?.data {
                    KeychainService.setUserGUID(value: id)
                }
            } else {
                print(error)
            }
            
            completionHandler(checkRes,error)
            //if RegisterService.showError(vc, checkRes, error) {
            
            // }
        }
    }
}
