//
//  ViewController.swift
//  asitakipIOS
//
//  Created by Mac on 25.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit
import MBProgressHUD

class LoginViewController: BaseVC {
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var showHideButton: UIButton!
    
    var iconClick: Bool = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.leftNavigationItem = .back
        self.hideKeyboardWhenTappedAround()
        title = "Giriş"
    }
    
    @IBAction func emailText(_ sender: UITextField) {
        let email = isValidEmail(testStr: emailText.text!)
        
        if email == false {
            print("geçersiz mail")
        } else {
            print("geçerli mail")
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    @IBAction func loginButton(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        LoginService.login(loginJson() , vc: self) { (checkResp, error) in
            if checkResp != nil
            {
                if !checkResp!.success {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.alert(message: checkResp!.message)
                    print("Hata")
                } else {
                    KeychainService.setActivationCompleted(value: true)
                    print("Başarılı")
                    MBProgressHUD.hide(for: self.view, animated: true)
                    let vc = HomeViewController.instantiate()
                    self.show(vc, sender: nil)
                }
            } else {
                print("error")
            }
        }
    }
    
    private func loginJson() -> [String:String]
    {
        return [
            "email": emailText.text ?? "",
            "password": passwordText.text ?? ""]
    }
    
    @IBAction func showHideAction(_ sender: Any) {
        if iconClick == true {
            passwordText.isSecureTextEntry = false
            showHideButton.setImage(#imageLiteral(resourceName: "icon2"), for: .normal)
            iconClick = false
        } else {
            passwordText.isSecureTextEntry = true
            showHideButton.setImage(#imageLiteral(resourceName: "icon1"), for: .normal)
            iconClick = true
        }
    }
}

extension LoginViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .login }
}
