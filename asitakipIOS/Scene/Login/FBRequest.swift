////
////  Request.swift
////  asitakipIOS
////
////  Created by Mac on 7.04.2020.
////  Copyright © 2020 Samet Dogru. All rights reserved.
////
//
//import Foundation
//import SwiftyJSON
//
////struct Response: GraphResponseProtocol {
////  init(rawResponse: Any?) {
////    // Decode JSON from rawResponse into other properties here.
////  }
////}
//
//
//struct FBProfileRequest {
//
//  var graphPath = "/me"
//  var parameters: [String : Any]? =  ["fields": "id, name, first_name, last_name, age_range, gender, locale, timezone, picture"]
//    var accessToken = AccessToken.current
//    //var httpMethod: GraphRequestHTTPMethod = .GET
//    //var apiVersion: GraphAPIVersion = .defaultVersion
//}
//
//extension FBProfileRequest {
//
//    static func getFBUserInfo(completionHandler:@escaping(_ user:User?) -> ())  {
//
//        //let params = ["fields": "id, name, first_name, last_name, gender, locale, picture"]
//        let params = ["fields": "id, name, first_name, last_name,email"]
//        let graphRequest = GraphRequest(graphPath: "me", parameters: params)
//
//        graphRequest.start {(connection, result, error) in
//            if error == nil
//            {
//                if let responseDictionary = result as? NSDictionary
//                {
//                    log.debug(responseDictionary)
//                    let user = User()
//                    if let firstNameFB = responseDictionary["first_name"] as? String {
//                        user.name = firstNameFB
//                    }
//                    if let lastNameFB = responseDictionary["last_name"] as? String {
//                        user.surname = lastNameFB
//                    }
//                    if let email = responseDictionary["email"] as? String {
//                        user.email = email
//                    }
//
//                    completionHandler(user)
//                    return
//                }
//                 completionHandler(nil)
//            }else{
//                   log.debug("error in graph request:", error!.localizedDescription)
//                completionHandler(nil)
//            }
//        }
//    }
//}
//
//
//
