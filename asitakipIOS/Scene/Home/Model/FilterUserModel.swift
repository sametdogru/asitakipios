//
//  FilterUserModel.swift
//  asitakipIOS
//
//  Created by Mac on 21.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation

struct FilterUserModel {
    var subUserGuid:String?
    var name:String?
    var surname:String?
    var city:String?
    var gender:String?
    var birthDate: String?
    var diseases: [Int]?
    var professionId: Int?
}
