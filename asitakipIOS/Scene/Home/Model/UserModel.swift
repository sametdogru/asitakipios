//
//  UserModel.swift
//  asitakipIOS
//
//  Created by Mac on 4.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//


import Foundation
import SwiftyJSON


//Mark: - Users

struct Users: Codable,JSONCollectionSerializable {

    var subUserGuid:String?
    var name:String?
    var surname:String?
    var city:String?
    var gender:String?
    var birthDate: String?
    var diseases: [Int]?
    var professionId: Int?
    
    init(json: JSON) {
           subUserGuid = json["subUserGuid"].stringValue
           city = json["city"].stringValue
           name = json["name"].stringValue
           surname = json["surname"].stringValue
           gender = json["gender"].stringValue
           birthDate = json["birthDate"].stringValue
           professionId = json["professionId"].intValue
           diseases = json["diseases"].arrayObject as? [Int]
       }
}
