//
//  RegisterService.swift
//  asitakipIOS
//
//  Created by Mac on 2.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import SwiftyJSON


struct UserService {
    
    static func getUser(_ parameter:[String:Any],vc:BaseVC? = nil,completionHandler: @escaping ( _ respone:[Users]?, _ error:MyError?) -> Swift.Void){
        let urlStr = String(format: Service.Users, parameter)
        Network.shared.post(url: urlStr,parameters: parameter, showIndicator:true) {  (response,error) in
            
            if error == nil
            {
                completionHandler(Users.getCollection(json: response["data"]),nil)
            } else {
                completionHandler(nil,error)
            }
        }
    }
}


