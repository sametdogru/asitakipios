//
//  HomeCollectionViewCell.swift
//  asitakipIOS
//
//  Created by Mac on 21.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func configure(item: [FilterUserModel], index: Int) {
        self.nameLbl.text = "\(item[index].name ?? "") \(item[index].surname ?? "")".initials
    }
}

