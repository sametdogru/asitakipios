//
//  HomeTableViewCell.swift
//  asitakipIOS
//
//  Created by Mac on 26.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

protocol HomeAddProtocol {
    func didTapButton(with tag: Int)
}

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var addButton: UIButton!
    
    var delegate: HomeAddProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func addButton(_ sender: AnyObject) {
        self.delegate?.didTapButton(with: sender.tag)
    }
    
    func tableViewConfig(index: Int) {
        img.isHidden = true
        img.isUserInteractionEnabled = true
        view.isUserInteractionEnabled = true
        addButton.isHidden = false
        addButton.tag = index
        collectionView.tag = index
        collectionView.reloadData()
    }
    
    func collectionViewConfig(vc: UIViewController) {
        self.collectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCell")
        self.collectionView.delegate = vc as? UICollectionViewDelegate
        self.collectionView.dataSource = vc as? UICollectionViewDataSource
    }
}
