//
//  HomeViewController.swift
//  asitakipIOS
//
//  Created by Mac on 26.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class HomeViewController: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    
    private lazy var settingsButton: UIBarButtonItem = {
        let button = UIBarButtonItem(image: #imageLiteral(resourceName: "settings"), style: .plain , target: self, action: #selector(HomeViewController.settings))
        return button
    }()
    
    var titleArray = ["Rutin Aşılar","Çocuk Aşısı","","",""]
    var isRegister: Bool = false
    var subUserDetails:[Users]?
    var adultUserArray = [FilterUserModel]()
    var childUserArray = [FilterUserModel]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        adultUserArray.removeAll()
        childUserArray.removeAll()
        getUser()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        tableView.reloadData()
//    }
    
    func initUI() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        self.leftNavigationItem = .none
        navigationItem.rightBarButtonItem = settingsButton
        title = "Anasayfa"
        
        if isRegister {
            let vc = RegisterSuccessViewController.instantiate()
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    private func getUser() {
        
        let param = ["userGuid": KeychainService.getUserGUID() ?? ""] as [String : Any]
        UserService.getUser(param, vc: self) { (users, error) in
            self.subUserDetails = users
            self.filterUser()
        }
    }
    
    private func filterUser() {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        if self.subUserDetails?.count ?? 0 > 0 {
            
            self.subUserDetails?.forEach({
                print($0.birthDate ?? "")
                let year = dateFormatter.date(from: $0.birthDate ?? "")
                let yearComponent = Calendar.current.dateComponents([.year], from: year!, to: Date())
                let yearly = yearComponent.year ?? 0
                
                if yearly > 18 || yearly == 18 {
                    let adult = FilterUserModel.init(subUserGuid: $0.subUserGuid, name: $0.name, surname: $0.surname, city: $0.city, gender: $0.gender, birthDate: $0.birthDate, diseases: $0.diseases, professionId: $0.professionId)
                    self.adultUserArray.append(adult)
                } else {
                    let child = FilterUserModel.init(subUserGuid: $0.subUserGuid, name: $0.name, surname: $0.surname, city: $0.city, gender: $0.gender, birthDate: $0.birthDate, diseases: $0.diseases, professionId: $0.professionId)
                    self.childUserArray.append(child)
                }
            })
            tableView.reloadData()
        } else {
            print("subUserDetails nil")
        }
    }
    
    @objc func settings() {
        let vc = SettingsViewController.instantiate()
        vc.subUserDetails = self.subUserDetails?.first
        self.show(vc, sender: nil)
    }
}

extension HomeViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .main }
}

extension HomeViewController: HomeAddProtocol {
    func didTapButton(with tag: Int) {
        if tag == 0 {
            let vc = AddAdultViewController.instantiate()
            vc.isRegister = false
            self.show(vc, sender: nil)
        } else if tag == 1 {
            let vc = AddChildViewController.instantiate()
            vc.isRegister = false
            self.show(vc, sender: nil)
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTableViewCell
        
        cell.tableViewConfig(index: indexPath.row)
        cell.title.text = titleArray[indexPath.row]
        cell.delegate = self
        
        switch indexPath.row {
        case 0:
            cell.collectionViewConfig(vc: self)
            cell.view.backgroundColor = UIColor(red: 10/255, green: 57/255, blue: 151/255, alpha: 1.0)
        case 1:
            cell.collectionViewConfig(vc: self)
            cell.view.backgroundColor = UIColor(red: 85/255, green: 64/255, blue: 251/255, alpha: 1.0)
//        case 2:
//            cell.collectionViewConfig(vc: self)
//            cell.view.backgroundColor = UIColor(red: 255/255, green: 187/255, blue: 56/255, alpha: 1.0)
        case 2:
            cell.img.isHidden = false
            cell.img.isUserInteractionEnabled = false
            cell.view.isUserInteractionEnabled = false
            cell.addButton.isHidden = true
            cell.img.image = UIImage(named: "containerContentCopy5")
        case 3:
            cell.img.isHidden = false
            cell.img.isUserInteractionEnabled = false
            cell.view.isUserInteractionEnabled = false
            cell.addButton.isHidden = true
            cell.img.image = UIImage(named: "containerContentCopy2")
        case 4:
            cell.img.isHidden = false
            cell.img.isUserInteractionEnabled = false
            cell.view.isUserInteractionEnabled = false
            cell.addButton.isHidden = true
            cell.img.image = UIImage(named: "containerContentCopy3")
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 2:
            let vc = TraumaFirstQuestionViewController.instantiate()
            self.show(vc, sender: nil)
        case 3:
            let vc = RecommendedVaccinesViewController.instantiate()
            self.show(vc, sender: nil)
        case 4:
            let vc = BeforeTravelViewController.instantiate()
            self.show(vc, sender: nil)
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 0 {
            return adultUserArray.count
        } else if collectionView.tag == 1 {
            return childUserArray.count
        }
        return Int()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        
        if collectionView.tag == 0 {
            cell.configure(item: adultUserArray, index: indexPath.row)
        } else if collectionView.tag == 1 {
            cell.configure(item: childUserArray, index: indexPath.row)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = VaccineCalendarViewController.instantiate()
        
        if collectionView.tag == 0 {
            let adultUser = adultUserArray[indexPath.row]
            vc.userDetails = adultUser
            vc.isAdult = true
            self.show(vc, sender: nil)
            
        } else if collectionView.tag == 1 {
            let childuser = childUserArray[indexPath.row]
            vc.userDetails = childuser
            vc.isAdult = false
            self.show(vc, sender: nil)
        }
    }
}

extension String {
    public var initials: String {
        var finalString = String()
        var words = components(separatedBy: .whitespacesAndNewlines)
        
        if let firstCharacter = words.first?.first {
            finalString.append(String(firstCharacter))
            words.removeFirst()
        }
        
        if let lastCharacter = words.last?.first {
            finalString.append(String(lastCharacter))
        }
        return finalString.uppercased()
    }
}
