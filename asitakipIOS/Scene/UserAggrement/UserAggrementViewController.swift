//
//  UserAggrementViewController.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 20.01.2021.
//  Copyright © 2021 Samet Dogru. All rights reserved.
//

import UIKit
import WebKit

class UserAggrementViewController: BaseVC {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.load(URLRequest(url: URL(string: "https://www.markasci.com/asi/kullanici-sozlesmesi")!))
    }
    
    @IBAction func closeBtn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension UserAggrementViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .register }
}
