//
//  ViewController.swift
//  Nevus
//
//  Created by Samet Dogru on 24.07.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class OnBoardingVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!

    var images = ["group", "group2", "group3", "group4"]
    var titleArray = ["Hoş Geldiniz!","Aşı Takibi","Önlem Alın","Sağlıkla Seyehat Edin"]
    var subTitleArray = ["Vaxsci ile tüm sevdiklerinizin sağlığını korumanız mümkün.","Çocuklarınızın ve evcil hayvanlarınızın aşılarını takip edin.","Kendinizin ve sevdiklerinizin sağlığını korumak için ihtiyacınız olabilecek aşı bilgilerine ulaşın.","Yurt dışı seyahatiniz öncesi ihangi sağlık önlemlerine ihtiyacınız olduğunu kolayca öğrenin."]

    static let defaultKey = "Vaxsci"
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.navigationController?.navigationBar.isHidden = true
        setup()
        UserDefaults.standard.set(true, forKey: OnBoardingVC.defaultKey)
    }

    func setup() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = .zero
        layout.itemSize = CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height)
        self.collectionView.collectionViewLayout = layout
        self.collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        pageControl.numberOfPages = self.images.count
        pageControl.pageIndicatorTintColor = .gray
        pageControl.currentPageIndicatorTintColor = .systemBlue
    }

    @IBAction func okButton(_ sender: UIButton) {
        print("SignIn")
        
        let vc = MainViewController.instantiate()
        self.show(vc, sender: nil)
    }
}

extension OnBoardingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        let img = images[indexPath.row]
        cell.img.image = UIImage(named: img)
        cell.titleLbl.text = titleArray[indexPath.row]
        cell.subTitleLbl.text = subTitleArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = self.collectionView.contentOffset
        visibleRect.size = self.collectionView.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = self.collectionView.indexPathForItem(at: visiblePoint) else { return }
        self.pageControl.currentPage = indexPath.item
    }
}

extension OnBoardingVC: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .main }
}

extension OnBoardingVC {
    
    static func showBoarding() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: OnBoardingVC.defaultKey)
    }
    static func alreadyShowed() -> Bool {
        
        let defaults = UserDefaults.standard
        if let _  = defaults.object(forKey: OnBoardingVC.defaultKey) {
            return true
        }
        return false
    }
}
