//
//  CollectionViewCell.swift
//  Nevus
//
//  Created by Samet Dogru on 24.07.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
