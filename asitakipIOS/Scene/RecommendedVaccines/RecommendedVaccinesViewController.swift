//
//  RecommendedVaccinesViewController.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 20.01.2021.
//  Copyright © 2021 Samet Dogru. All rights reserved.
//

import UIKit

class RecommendedVaccinesViewController: BaseVC {

    @IBOutlet weak var txtDescription: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Hac/Umre Öncesi"
        self.leftNavigationItem = .back
        
        txtDescription.text = "Rutin aşılar: Her yolculuk öncesi rutin aşılarınızın güncel olduğundan emin olun. Bu aşılar: Kızamık-Kızamıkçık-Kabakulak(MMR), Difteri, Tetanoz, Boğmaca, Su çiçeği, Polio ( çocuk felci ) ve yıllık grip aşısıdır.\n" +
            "\n" +
            "Kızamık birçok ülkede görülebildiğinden  ve son yıllarda dünya çapında salgınlar meydana geldiğinden, seyahate çıkmadan önce kızamığa karşı bağışıklık durumunuzdan emin olun.\n" +
            "\n" +
            "Hepatit A aşısı: Afganistan’da yediğiniz veya kalacağınız yer neresi olursa olsun, kontamine  su veya yiyeceklerle Hepatit A bulaşabileceğinden bu aşı önerilir.\n" +
            "\n" +
            "Tifo aşısı: Afganistan’da kontamine yiyecek ya da sularla tifo bulaşabilir. Bu aşı,  tifo riskinin  yüksek olduğu endemik bölgelerde , küçük şehir veya kırsal alanları ziyaret edecek, arkadaş veya akrabalarının evinde ikamet edecek yolculara ve yemek gezginlerine, özellikle 1 aydan fazla kalmaları durumunda önerilebilir.\n" +
            "\n" +
            "Hepatit B aşısı: Hepatit B kontamine iğneler, kan ürünleri ve cinsel temasla bulaşır. Bu nedenle herhangi bir tıbbi işlem, dövme veya piercing yaptıracaksanız, yeni bir partnerle cinsel temas ihtimali varsa aşı yaptırmanız önerilir.\n" +
            "\n" +
            "Kuduz aşısı: Kuduz, Afganistan’da  köpekler, yarasalar ve diğer memelilerde bulunabilir. Aşı özellikle aşağıdaki gruplara önerilir:\n" +
            "\n" +
            "·  Hayvanlar tarafından ısırılma riski oluşturacak dış mekan aktiviteleri ve diğer faaliyetlerde  bulunacak  (kampçılık, yürüyüş, bisiklet, macera seyahati ve mağaracılık gibi )  gezginler.\n" +
            "\n" +
            "·  Hayvanlarla veya hayvanlar etrafında çalışacak kişiler (veterinerler, vahşi yaşam uzmanları  ve araştırmacılar gibi).\n" +
            "\n" +
            "·  Afganistan’a uzun yolculuk yapacak veya bu ülkedeki uzak bölgelere taşınacak olan kişiler.\n" +
            "\n" +
            "·  Hayvanlarla oynama eğiliminde oldukları, ısırıldıklarını söylemeyebildikleri ve baş- boyun ısırıkları daha fazla olabildiği için çocuklar.\n" +
            "\n" +
            "Sarı Humma aşısı (2019):\n" +
            "\n" +
            "Ülkeye giriş gerekliliği: Hiçbir uluslararası yolcudan sarıhumma  aşı sertifikası istenmez.\n" +
            "\n" +
            "DSÖ sarıhumma aşı önerisi:  Yok.  \n" +
            "\n" +
            "Ülke için diğer gereklilik: (2019) Çocuk felci endemik ülkeden gelen yolcular için çocuk felci aşı belgesi gereklidir. Sakinleri  veya   Afganistan'da 4 haftadan fazla kalan gezginler için Afganistan'dan ayrılırken çocuk felci aşısı kanıtı gerekli olabilir. Bu aşı, Afganistan'dan ayrılma tarihinden önce,  4 hafta ila 12 ay arasında alınmış olmalıdır.\n" +
            "\n" +
            "Sıtma(2019): Sıtma tehlikesi (P. vivax ve P. falciparum kaynaklı) Mayıs ile Aralık ayları arasında 2000 metrenin altındaki yerlerde mevcuttur.\n" +
            "\n" +
            "P. falciparum kaynaklı sıtma riski olan bölgeler ziyaret edilecekse sivrisinek ısırıklarına karşı önlemlere ilave olarak;\n" +
            "\n" +
            "Meflokin 250mg tablet haftalık (haftada 1x250 mg tablet alınmak üzere seyahate çıkmadan 1 hafta önce başlanır, seyahat boyunca ve seyahatten sonra 4 hafta daha devam edilir.) ya da\n" +
            "\n" +
            "Atovaquone/Proguanil tablet  günde  1 kez 250/100mg olmak üzere yolculuktan 1-2 gün önce başlanır. Seyahat süresince ve döndükten sonra 1 hafta devam edilir ya da\n" +
            "\n" +
            "Doksisiklin 100mg kapsül günlük (günde 1x100 mg tablet alınmak üzere seyahate çıkmadan 1 gün önce başlanır, seyahat boyunca ve seyahatten sonra 4 hafta daha devam edilir).\n" +
            "\n" +
            "Alternatif olarak, sıtma enfeksiyonu riski düşük kırsal bölgelere seyahat edenler için, sivrisinek ısırığı önlemleri yardımcı acil tedavi ile kombine edilebilir.\n" +
            "\n" +
            "Not: İlaçlar mutlaka hekim önerisiyle kullanılmalıdır."
    }
}

extension RecommendedVaccinesViewController: StoryboardInstatiate,Reusable {
    public static var storyboard: Storyboard { return .travel }
}
