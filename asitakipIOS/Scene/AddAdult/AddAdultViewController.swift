//
//  AddAdultViewController.swift
//  asitakipIOS
//
//  Created by Mac on 26.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class AddAdultViewController: BaseVC {
    
    @IBOutlet weak var femaleRadioButton: UIButton!
    @IBOutlet weak var maleRadioButton: UIButton!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var surnameText: UITextField!
    @IBOutlet weak var professionText: UITextField!
    @IBOutlet weak var diseasesText: UITextField!
    @IBOutlet weak var dateText: UITextField!
    @IBOutlet weak var registerButton: Button!
    
    var isRegister: Bool = false
    var isSelectRadioButton: Bool = false
    var gender = String()
    var professionIndex = -1
    var diseasesIndex = [Int]()
    var diseasesArray = [String]()
    var vaccineDate = String()
    
    var subUser:SubUsers = SubUsers()
    var professions:[Professions]?
    var diseases: [Diseases]?
    var subUserDetails:[Users]?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getProfessions()
        getDiseases()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    func initUI() {
        title = "Profil"
        self.leftNavigationItem = .back
        self.hideKeyboardWhenTappedAround()

        if isRegister {
            title = "Profil"
            registerButton.setTitle("Kaydet", for: .normal)

            nameText.text = subUserDetails?.first?.name
            surnameText.text = subUserDetails?.first?.surname
            vaccineDateformatter(date: subUserDetails?.first?.birthDate ?? "")
            dateText.text = vaccineDate
            
            if subUserDetails?.first?.gender == "E" {
                isSelectRadioButton = true
                maleRadioButton.isSelected = false
                maleRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
                gender = "E"
            } else {
                isSelectRadioButton = true
                femaleRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
                gender = "K"
            }
        } else {
            title = "Yetişkin Ekle"
            registerButton.setTitle("Kaydol", for: .normal)
            //
            //
        }
    }
    
    func getProfessions() {
        ProfessionService.getProfessions {[weak self] (professions, error) in
            self?.professions = professions
            
            if self?.isRegister == true {
                self?.professionText.text = String(describing: self?.professions?[self?.subUserDetails?.first?.professionId ?? 0].name ?? "")
            }
        }
    }
    
    func getDiseases() {
        DiseaseService.getDiseases {[weak self] (diseases, error) in
            self?.diseases = diseases
                self?.subUserDetails?.first?.diseases?.forEach({ (disease) in
                    self?.diseasesArray.append(diseases?[disease].name ?? "")
                })
                self?.diseasesText.text = self?.diseasesArray.joined(separator: ", ")
        }
    }
    
    @IBAction func femaleRadioButton(_ sender: AnyObject) {
        if femaleRadioButton.isSelected == false {
            isSelectRadioButton = true
            femaleRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            maleRadioButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
            gender = "K"
        }
    }
    
    @IBAction func maleRadioButton(_ sender: Any) {
        if maleRadioButton.isSelected == false {
            isSelectRadioButton = true
            maleRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            femaleRadioButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
            gender = "E"
        }
    }
    
    @IBAction func dateText(_ sender: UITextField) {
        sender.resignFirstResponder()
        let vc = DatePickerController.instantiate()
        vc.delegate = self
        vc.isPerson = true
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func professionSelect(_ sender: UITextField) {
        sender.resignFirstResponder()
        let vc = ProfessionPickerController.instantiate()
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func diseasesSelect(_ sender: UITextField) {
        sender.resignFirstResponder()
        let vc = DiseasesPickerController.instantiate()
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func vaccineDateformatter(date:String) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let vaccinedate = dateFormatter.date(from: date)
        let vaccineDateFormatter = DateFormatter()
        vaccineDateFormatter.dateFormat = "dd.MM.yyyy"
        vaccineDate = vaccineDateFormatter.string(from: vaccinedate ?? Date())
    }
    
    @IBAction func registerButton(_ sender: Any) {
        if let name = nameText.text, let surname = surnameText.text, let profession = professionText.text, let diseases = diseasesText.text, let date = dateText.text, surname.isEmpty || name.isEmpty || profession.isEmpty || diseases.isEmpty || date.isEmpty || isSelectRadioButton == false {
            self.alert(message: "Lütfen boş alanları doldurunuz..")
        } else {
            registerAdultData()
        }
    }
    
    func registerAdultData() {
        subUser.userGuid = KeychainService.getUserGUID()
        subUser.subUserGuid = subUserDetails?.first?.subUserGuid
        subUser.name = nameText.text
        subUser.surname = surnameText.text
        subUser.city = "Ankara"
        subUser.gender = gender
        formatter(date: vaccineDate)
        subUser.birthDate = vaccineDate

        if diseasesIndex.isEmpty == false {
            subUser.diseases = diseasesIndex
        } else {
            subUser.diseases = subUserDetails?.first?.diseases
        }
        
        if professionIndex != -1 {
            subUser.profession = professionIndex
        } else {
            subUser.profession = subUserDetails?.first?.professionId
        }

        registerAdultUser()
    }
    
    private func registerAdultUser() {
        RegisterSubUserService.subUser(subUser.toDictionary() , vc: self) {(checkResp, error) in
            if checkResp != nil
            {
                if !checkResp!.success {
                    print("Error")
                    self.alert(message: checkResp!.message)
                }else{
                    print(checkResp!.message)
                    if self.isRegister {
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        self.getSubUser(subUserGuid: checkResp!.data!)
                    }
                }
            }
        }
    }
    
    private func getSubUser(subUserGuid: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let year = dateFormatter.date(from: self.vaccineDate)
        let yearComponent = Calendar.current.dateComponents([.year], from: year!, to: Date())
        let yearly = yearComponent.year ?? 0
        
        if yearly > 18 || yearly == 18 {
            let vc = QuestionPageAdultViewController.instantiate()
            vc.subUserGuid = subUserGuid
            self.show(vc, sender: nil)
        } else {
            let vc = HomeViewController.instantiate()
            self.show(vc, sender: nil)
        }
    }
}

extension AddAdultViewController: DatePickerProtocol {
    func selectedDate(date: String) {
        formatter(date: date)
        dateText.text = date
    }
    
    func formatter(date:String) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        if let vaccinedate = dateFormatter.date(from: date) {
            let vaccineDateFormatter = DateFormatter()
            vaccineDateFormatter.dateFormat = "yyyy-MM-dd"
            self.vaccineDate = vaccineDateFormatter.string(from: vaccinedate)
        }
    }
}

extension AddAdultViewController: ProfessionPickerProtocol {
    func selectedProfession(profession: String, index: Int) {
        self.professionText.text = profession
        self.professionIndex = index
    }
}

extension AddAdultViewController: DiseasesPickerProtocol {
    func selectedDiseases(diseases: [String], index: [Int]) {
        self.diseasesText.text = diseases.joined(separator: ", ")
        self.diseasesIndex = index
    }
}

extension AddAdultViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .register }
}


//extension String {
//
//    func formatter(date:String) -> String {
//        let dateFormatter = DateFormatter()
//        var vaccineDate = String()
//        dateFormatter.locale = .init(identifier: "tr_TR")
//        dateFormatter.dateFormat = "dd.MM.yyyy"
//
//        if let vaccinedate = dateFormatter.date(from: date) {
//            let vaccineDateFormatter = DateFormatter()
//            vaccineDateFormatter.dateFormat = "yyyy-MM-dd"
//            vaccineDate = vaccineDateFormatter.string(from: vaccinedate)
//        }
//        return vaccineDate
//    }
//}
