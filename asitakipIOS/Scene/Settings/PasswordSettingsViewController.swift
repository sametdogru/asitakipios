//
//  PasswordSettingsViewController.swift
//  asitakipIOS
//
//  Created by Mac on 27.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class PasswordSettingsViewController: BaseVC {
    
    @IBOutlet weak var oldPassword: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmNewPassword: UITextField!
    @IBOutlet weak var firstShowHideButton: UIButton!
    @IBOutlet weak var secondShowHideButton: UIButton!
    @IBOutlet weak var thirdShowHideButton: UIButton!
    
    var firstShowHide: Bool = true
    var secondShowHide: Bool = true
    var thirdShowHide: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Şifre Değiştir"
        self.leftNavigationItem = .back
        // Do any additional setup after loading the view.
    }
    
    @IBAction func firstShowHideButton(_ sender: UIButton) {
        if firstShowHide == true {
            oldPassword.isSecureTextEntry = false
            firstShowHideButton.setImage(#imageLiteral(resourceName: "icon2"), for: .normal)
            firstShowHide = false
        } else {
            oldPassword.isSecureTextEntry = true
            firstShowHideButton.setImage(#imageLiteral(resourceName: "icon1"), for: .normal)
            firstShowHide = true
        }
    }
    
    @IBAction func secondShowHideButton(_ sender: UIButton) {
        if secondShowHide == true {
            newPassword.isSecureTextEntry = false
            secondShowHideButton.setImage(#imageLiteral(resourceName: "icon2"), for: .normal)
            secondShowHide = false
        } else {
            newPassword.isSecureTextEntry = true
            secondShowHideButton.setImage(#imageLiteral(resourceName: "icon1"), for: .normal)
            secondShowHide = true
        }
    }
    
    @IBAction func thirdShowHideButton(_ sender: UIButton) {
        if thirdShowHide == true {
            confirmNewPassword.isSecureTextEntry = false
            thirdShowHideButton.setImage(#imageLiteral(resourceName: "icon2"), for: .normal)
            thirdShowHide = false
        } else {
            confirmNewPassword.isSecureTextEntry = true
            thirdShowHideButton.setImage(#imageLiteral(resourceName: "icon1"), for: .normal)
            thirdShowHide = true
        }
    }
}

extension PasswordSettingsViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { .settings }
}
