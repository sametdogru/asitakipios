//
//  LogoutViewController.swift
//  asitakipIOS
//
//  Created by Mac on 1.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit
//protocol LogoutProtocol {
//    func logOut()
//}

class LogoutViewController: BaseVC {

    //var delegate: LogoutProtocol?
    var nav: UINavigationController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func logoutBtn(_ sender: Any) {
        self.dismiss(animated: true) {
            CoreManager.instance.logout()
            let vc = MainViewController.instantiate()
            self.nav?.pushViewController(vc, animated: false)
        }
    }
}

extension LogoutViewController: StoryboardInstatiate,Reusable {
    public static var storyboard: Storyboard { return .settings }
}
