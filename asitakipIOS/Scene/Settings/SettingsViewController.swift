//
//  SettingsViewController.swift
//  asitakipIOS
//
//  Created by Mac on 27.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class SettingsViewController: BaseVC {
    var subUserDetails:Users?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Ayarlar"
        self.leftNavigationItem = .back
    }
    
    @IBAction func editProfileButton(_ sender: Any) {
        let vc = ProfileSettingsViewController.instantiate()
        vc.subUserDetails = self.subUserDetails
        self.show(vc, sender: nil)
    }
    
    @IBAction func changePasswordButton(_ sender: Any) {
        let vc = PasswordSettingsViewController.instantiate()
        self.show(vc, sender: nil)
    }
    @IBAction func logoutButton(_ sender: Any) {
        let vc = LogoutViewController.instantiate()
        vc.nav = self.navigationController
        self.present(vc, animated: true, completion: nil)
    }
}

extension SettingsViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { .settings }
}
