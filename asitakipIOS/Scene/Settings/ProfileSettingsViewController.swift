//
//  ProfileSettingsViewController.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 8.11.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class ProfileSettingsViewController: BaseVC {
    
    @IBOutlet weak var femaleRadioButton: UIButton!
    @IBOutlet weak var maleRadioButton: UIButton!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var surnameText: UITextField!
    @IBOutlet weak var professionText: UITextField!
    @IBOutlet weak var diseasesText: UITextField!
    @IBOutlet weak var dateText: UITextField!
    @IBOutlet weak var updateButton: Button!
    
    var isSelectRadioButton: Bool = false
    var gender = String()
    var professionIndex = -1
    var diseasesIndex = [Int]()
    var diseasesArray = [String]()
    var vaccineDate = String()
    
    var subUser:SubUsers = SubUsers()
    var professions:[Professions]?
    var diseases: [Diseases]?
    var subUserDetails:Users?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getProfessions()
        getDiseases()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    func initUI() {
        self.leftNavigationItem = .back
        title = "Profil"
        nameText.text = subUserDetails?.name
        surnameText.text = subUserDetails?.surname
        vaccineDateformatter(date: subUserDetails?.birthDate ?? "")
        dateText.text = vaccineDate
        
        if subUserDetails?.gender == "E" {
            isSelectRadioButton = true
            maleRadioButton.isSelected = false
            maleRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            gender = "E"
        } else {
            isSelectRadioButton = true
            femaleRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            gender = "K"
        }
    }
    
    func getProfessions() {
        ProfessionService.getProfessions {[weak self] (professions, error) in
            self?.professions = professions
                self?.professionText.text = String(describing: self?.professions?[self?.subUserDetails?.professionId ?? 0].name ?? "")
            
        }
    }
    
    func getDiseases() {
        DiseaseService.getDiseases {[weak self] (diseases, error) in
            self?.diseases = diseases
                self?.subUserDetails?.diseases?.forEach({ (disease) in
                    self?.diseasesArray.append(diseases?[disease].name ?? "")
                })
                self?.diseasesText.text = self?.diseasesArray.joined(separator: ", ")
        }
    }
    
    @IBAction func femaleRadioButton(_ sender: AnyObject) {
        if femaleRadioButton.isSelected == false {
            isSelectRadioButton = true
            femaleRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            maleRadioButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
            gender = "K"
        }
    }
    
    @IBAction func maleRadioButton(_ sender: Any) {
        if maleRadioButton.isSelected == false {
            isSelectRadioButton = true
            maleRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            femaleRadioButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
            gender = "E"
        }
    }
    
    @IBAction func dateText(_ sender: UITextField) {
        sender.resignFirstResponder()
        let vc = DatePickerController.instantiate()
        vc.delegate = self
        vc.isPerson = true
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func professionText(_ sender: UITextField) {
        sender.resignFirstResponder()
        let vc = ProfessionPickerController.instantiate()
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func diseasesText(_ sender: UITextField) {
        sender.resignFirstResponder()
        let vc = DiseasesPickerController.instantiate()
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func vaccineDateformatter(date:String) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let vaccinedate = dateFormatter.date(from: date)
        let vaccineDateFormatter = DateFormatter()
        vaccineDateFormatter.dateFormat = "dd.MM.yyyy"
        vaccineDate = vaccineDateFormatter.string(from: vaccinedate ?? Date())
    }
    
    @IBAction func updateButton(_ sender: Any) {
        if let name = nameText.text, let surname = surnameText.text, let profession = professionText.text, let diseases = diseasesText.text, let date = dateText.text, surname.isEmpty || name.isEmpty || profession.isEmpty || diseases.isEmpty || date.isEmpty || isSelectRadioButton == false {
            self.alert(message: "Lütfen boş alanları doldurunuz..")
        } else {
            updateUser()
        }
    }
    
    func updateUser() {
        subUser.userGuid = KeychainService.getUserGUID()
        subUser.subUserGuid = subUserDetails?.subUserGuid
        subUser.name = nameText.text
        subUser.surname = surnameText.text
        subUser.city = "Ankara"
        subUser.gender = gender
        formatter(date: vaccineDate)
        subUser.birthDate = vaccineDate

        if diseasesIndex.isEmpty == false {
            subUser.diseases = diseasesIndex
        } else {
            subUser.diseases = subUserDetails?.diseases
        }
        
        if professionIndex != -1 {
            subUser.profession = professionIndex
        } else {
            subUser.profession = subUserDetails?.professionId
        }

        updateUserRequest()
    }
    
    private func updateUserRequest() {
        RegisterSubUserService.subUser(subUser.toDictionary() , vc: self) {(checkResp, error) in
            if checkResp != nil
            {
                if !checkResp!.success {
                    print("Error")
                    self.alert(message: checkResp!.message)
                }else{
                    print(checkResp!.message)
                        self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

extension ProfileSettingsViewController: DatePickerProtocol {
    func selectedDate(date: String) {
        formatter(date: date)
        dateText.text = date
    }
    
    func formatter(date:String) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        if let vaccinedate = dateFormatter.date(from: date) {
            let vaccineDateFormatter = DateFormatter()
            vaccineDateFormatter.dateFormat = "yyyy-MM-dd"
            self.vaccineDate = vaccineDateFormatter.string(from: vaccinedate)
        }
    }
}

extension ProfileSettingsViewController: ProfessionPickerProtocol {
    
    func selectedProfession(profession: String, index: Int) {
        self.professionText.text = profession
        self.professionIndex = index
    }
}

extension ProfileSettingsViewController: DiseasesPickerProtocol {
    func selectedDiseases(diseases: [String], index: [Int]) {
        self.diseasesText.text = diseases.joined(separator: ", ")
        self.diseasesIndex = index
    }
}

extension ProfileSettingsViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { .settings }
}
