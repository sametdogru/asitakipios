//
//  AddUserViewController.swift
//  asitakipIOS
//
//  Created by Mac on 27.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit
import MBProgressHUD

class AddChildViewController: BaseVC {
    
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var surnameText: UITextField!
    @IBOutlet weak var diseasesText: UITextField!
    @IBOutlet weak var dateText: UITextField!
    @IBOutlet weak var maleRadioButton: UIButton!
    @IBOutlet weak var femaleRadioButton: UIButton!
    
    var isRegister: Bool = false
    var isSelectRadioButton: Bool = false
    var vaccineDate = String()
    var gender = String()
    var diseasesIndex = [Int]()
    var diseasesArray = [String]()
    var diseases: [Diseases]?
    var subUser:SubUsers = SubUsers()
    var subUserDetails:[Users]?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getDiseases()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    func initUI() {
        
        if isRegister {
            title = "Profil"
            nameText.text = subUserDetails?.first?.name
            surnameText.text = subUserDetails?.first?.surname
            vaccineDateformatter(date: subUserDetails?.first?.birthDate ?? "")
            dateText.text = vaccineDate
            
            if subUserDetails?.first?.gender == "E" {
                isSelectRadioButton = true
                maleRadioButton.isSelected = false
                maleRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
                gender = "E"
            } else {
                isSelectRadioButton = true
                femaleRadioButton.isSelected = false
                femaleRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
                gender = "K"
            }
        } else {
            title = "Çocuk Ekle"
        }
        self.leftNavigationItem = .back
        self.hideKeyboardWhenTappedAround()
    }
    
    func getDiseases() {
        DiseaseService.getDiseases {[weak self] (diseases, error) in
            self?.diseases = diseases
            self?.subUserDetails?.first?.diseases?.forEach({ (disease) in
                self?.diseasesArray.append(diseases?[disease].name ?? "")
            })
            self?.diseasesText.text = self?.diseasesArray.joined(separator: ", ")
        }
    }
    
    @IBAction func selectDate(_ sender: UITextField) {
        sender.resignFirstResponder()
        let vc = DatePickerController.instantiate()
        vc.delegate = self
        vc.isPerson = false
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func femaleRadioButton(_ sender: AnyObject) {
        if femaleRadioButton.isSelected == false {
            isSelectRadioButton = true
            femaleRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            maleRadioButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
            gender = "K"
            print(gender)
        }
    }
    
    @IBAction func maleRadioButton(_ sender: Any) {
        if maleRadioButton.isSelected == false {
            isSelectRadioButton = true
            maleRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            femaleRadioButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
            gender = "E"
            print(gender)
        }
    }
    
    @IBAction func selectDiseases(_ sender: UITextField)
    {
        sender.resignFirstResponder()
        let vc = DiseasesPickerController.instantiate()
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func vaccineDateformatter(date:String)  {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let vaccinedate = dateFormatter.date(from: date)
        let vaccineDateFormatter = DateFormatter()
        vaccineDateFormatter.dateFormat = "dd.MM.yyyy"
        vaccineDate = vaccineDateFormatter.string(from: vaccinedate!)
    }
    
    @IBAction func saveButton(_ sender: Any) {
        if let name = nameText.text, let surname = surnameText.text, let diseases = diseasesText.text, let date = dateText.text, surname.isEmpty || name.isEmpty || diseases.isEmpty || date.isEmpty || isSelectRadioButton == false {
            self.alert(message: "Lütfen boş alanları doldurunuz..")
        } else {
            registerChildData()
        }
    }
    
    func registerChildData() {
        subUser.userGuid = KeychainService.getUserGUID()
        subUser.subUserGuid = subUserDetails?.first?.subUserGuid
        subUser.name = nameText.text
        subUser.surname = surnameText.text
        subUser.city = "Ankara"
        subUser.gender = gender
        formatter(date: vaccineDate)
        subUser.birthDate = vaccineDate

        if diseasesIndex.isEmpty == false {
            subUser.diseases = diseasesIndex
        } else {
            subUser.diseases = subUserDetails?.first?.diseases
        }
        
        registerChildUser()
    }
    
    private func registerChildUser() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        RegisterSubUserService.subUser(subUser.toDictionary() , vc: self) {(checkResp, error) in
            if checkResp != nil
            {
                if !checkResp!.success {
                    print("Error")
                    self.alert(message: checkResp!.message)
                } else {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

extension AddChildViewController: DatePickerProtocol {
    func selectedDate(date: String) {
        formatter(date: date)
        self.dateText.text = date
    }
    
    func formatter(date:String) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        if let vaccinedate = dateFormatter.date(from: date) {
            let vaccineDateFormatter = DateFormatter()
            vaccineDateFormatter.dateFormat = "yyyy-MM-dd"
            self.vaccineDate = vaccineDateFormatter.string(from: vaccinedate)
        }
    }
}

extension AddChildViewController: DiseasesPickerProtocol {
    func selectedDiseases(diseases: [String], index: [Int]) {
        self.diseasesText.text = diseases.joined(separator: ", ")
        self.diseasesIndex = index
    }
}

extension AddChildViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .register }
}
