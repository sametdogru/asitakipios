//
//  VaccineScheduleDateTableViewCell.swift
//  asitakipIOS
//
//  Created by Mac on 3.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

protocol VaccineCalendarDateTableViewCellProtocol {
    func addVaccineBtn()
}

class VaccineCalendarDateTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var ageText: UILabel!
    @IBOutlet weak var addVaccineBtn: UIButton!
    
    var vaccineDate = String()
    var delegate: VaccineCalendarDateTableViewCellProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ageText.numberOfLines = 0
        ageText.sizeToFit()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureChild(key: String, values: [SubUserCalendarModel], index: Int) {
        ageText.text = values[index].subText
        formatter(date: values[index].vaccineDate ?? "")
        dateText.text = self.vaccineDate
        addVaccineBtn.tag = index
    }
    
    func configureAdult(key: String, values: [SubUserCalendarModel], index: Int, birthDate:String) {
        
        formatter(date: values[index].vaccineDate ?? "")
        dateText.text = self.vaccineDate
        
        let dateFormatter = DateFormatter()
        let calendar = Calendar.current
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let startDate = dateFormatter.date(from: birthDate)
        let endDate = dateFormatter.date(from: key)
        
        let age = calendar.dateComponents([.year], from: startDate ?? Date(), to: endDate ?? Date())
        ageText.text = "\(age.year ?? 0) Yaşında"
        addVaccineBtn.tag = index
    }
    
    func formatter(date:String) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let vaccinedate = dateFormatter.date(from: date)
        let vaccineDateFormatter = DateFormatter()
        vaccineDateFormatter.dateFormat = "dd.MM.yyyy"
        self.vaccineDate = vaccineDateFormatter.string(from: vaccinedate ?? Date())
    }
    
    @IBAction func addVaccineBtn(_ sender: UIButton) {
        self.delegate?.addVaccineBtn()
    }
}


