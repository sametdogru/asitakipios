//
//  VaccineSheduleTableViewCell.swift
//  asitakipIOS
//
//  Created by Mac on 3.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

protocol VaccineCalendarTableViewCellProtocol {
    func checkButton(index: Int, value: Int)
    func dotButton(index: Int, value: Int, desc: String)
}

class VaccineCalendarTableViewCell: UITableViewCell {
 
    @IBOutlet weak var vaccineView: UIView!
    @IBOutlet weak var vaccineType: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var vaccineDescription: UILabel!
    @IBOutlet weak var dotButton: UIButton!
    
    let bgColor = UIColor(red: 93/255, green: 195/255, blue: 137/255, alpha: 1.0)
    let txtColor = UIColor(red: 97/255, green: 177/255, blue: 88/255, alpha: 1)
    var delegate: VaccineCalendarTableViewCellProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configureChild(key: String, values: [SubUserCalendarModel], index: Int) {
        if values[index].customVaccineDate == "" {
            unvaccinatedConfig()
        } else {
           vaccinatedConfig()
        }
        vaccineType.text = values[index].vaccineGroupName
    }
    
    func configureAdult(key: String, values: [SubUserCalendarModel], index:Int) {
        
        vaccineType.text = values[index].vaccineGroupName
        vaccineDescription.text = values[index].text
        
        if values[index].completed == false {
            unvaccinatedConfig()
        } else {
            vaccinatedConfig()
        }
    }
    
    func vaccinatedConfig() {
        self.vaccineType.textColor = .white
        self.vaccineDescription.textColor = .white
        self.vaccineView.backgroundColor = UIColor(red: 120/255, green: 192/255, blue: 142/255, alpha: 1)
        self.dotButton.tintColor = .white
        self.checkButton.tintColor = .white
        self.checkButton.isHidden = true
        self.checkButton.isSelected = true
    }
    
    func unvaccinatedConfig() {
        self.vaccineType.textColor = UIColor(red: 30/255, green: 144/255, blue: 255/255, alpha: 1.0)
        self.vaccineDescription.textColor = UIColor(red: 147/255, green: 147/255, blue: 147/255, alpha: 1.0)
        self.vaccineView.backgroundColor = .white
        self.dotButton.tintColor = UIColor(red: 30/255, green: 144/255, blue: 255/255, alpha: 1.0)
        self.checkButton.isHidden = false
        self.checkButton.isSelected = false
    }
    
    @IBAction func checkButton(_ sender: UIButton) {
        if checkButton.isSelected == false {
            vaccinatedConfig()
            self.delegate?.checkButton(index: sender.tag, value: self.tag)
        }
    }
    
    @IBAction func dotButton(_ sender: UIButton) {
        self.delegate?.dotButton(index: sender.tag, value: self.tag, desc: self.vaccineDescription.text ?? "")

    }
}
