//
//  VaccineScheduleCollectionViewCell.swift
//  asitakipIOS
//
//  Created by Mac on 3.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class VaccineCalendarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = .zero
        layer.shadowRadius = 15
        layer.cornerRadius = 15
        layer.shadowOpacity = 0.6
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect:contentView.bounds, cornerRadius:contentView.layer.cornerRadius).cgPath
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "VaccineCalendarTableViewCell", bundle: nil), forCellReuseIdentifier: "vaccineCell")
        tableView.register(UINib(nibName: "VaccineCalendarDateTableViewCell", bundle: nil), forCellReuseIdentifier: "vaccineCellDate")
        tableView.reloadData()
    }
}
