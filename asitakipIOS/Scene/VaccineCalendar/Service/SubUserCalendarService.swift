//
//  SubUserCalendarRequest.swift
//  asitakipIOS
//
//  Created by Mac on 5.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import SwiftyJSON

struct SubUserCalendarService {
    
    static func subUserCalendar(_ parameter:[String:Any],vc:BaseVC? = nil,completionHandler: @escaping ( _ respone:[SubUserCalendarModel]?, _ error:MyError?) -> Swift.Void){
        let urlStr = String(format: Service.SubUserCalendar, parameter)
        Network.shared.post(url: urlStr,parameters: parameter, showIndicator:true) {  (response,error) in
            print(response)
            
            if error == nil
            {
                completionHandler(SubUserCalendarModel.getCollection(json: response["data"]),nil)
            } else {
                completionHandler(nil,error)
            }
        }
    }
}

struct SubUserAdultCalendarService {
    
    static func subUserAdultCalendar(_ parameter:[String:Any],vc:BaseVC? = nil,completionHandler: @escaping ( _ respone:[SubUserCalendarModel]?, _ error:MyError?) -> Swift.Void){
        let urlStr = String(format: Service.SubUserAdultCalendar, parameter)
        Network.shared.post(url: urlStr,parameters: parameter, showIndicator:true) {  (response,error) in
            if error == nil
            {
                completionHandler(SubUserCalendarModel.getCollection(json: response["data"]),nil)
            } else {
                completionHandler(nil,error)
            }
        }
    }
}

//Mark: - Vaccines

struct Vaccines :Codable,JSONCollectionSerializable{
    let id:Int
    let name:String
    let shortcode:String
    let description:String

    init(json: JSON) {
        id = json["id"].intValue
        name = json["name"].stringValue
        shortcode = json["shortcode"].stringValue
        description = json["description"].stringValue
    }
}

struct VaccinesService {
    
    static func getVaccines(_ completionHandler: @escaping (_ cities: [Vaccines]?, _ error:MyError?) -> Swift.Void){
        let urlStr = String(describing: Service.Vaccines)
        Network.shared.get(url: urlStr) { (response, error) in
            
            if error == nil {
                print(response)
                completionHandler(Vaccines.getCollection(json: response["data"]),nil)
            } else {
                completionHandler(nil,error)
            }
        }
    }
}


