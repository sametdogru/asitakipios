//
//  VaccineScheduleViewController.swift
//  asitakipIOS
//
//  Created by Mac on 3.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout

class VaccineCalendarViewController: BaseVC {
     
     @IBOutlet weak var collectionView: UICollectionView!
     @IBOutlet weak var stackView: UIStackView!
     
     var age = String()
     var filterAge = Int()
     var initials = String()
     var hasVaccine = String()
     var vaccineDate = String()
     var navigationView = UIView()
     var subViewArray = [UIView]()
     var userDetails: FilterUserModel?
     var subUserDetails:[Users]?
     var isAdult: Bool = false
     
     let viewModel = VaccineCalendarViewModel()
     
     override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(true)
          self.getUser()
     }
     
     override func viewDidLoad() {
          super.viewDidLoad()
          collectionViewConfigure()
          self.leftNavigationItem = .back
          
          if isAdult {
               self.viewModel.getAdultUserCalendar(subUserGuid: self.userDetails?.subUserGuid)
          } else {
               self.viewModel.getChildUserCalendar(subUserGuid: self.userDetails?.subUserGuid)
          }
          
          self.viewModel.getVaccines()
          
          initViewModel()
     }
     
     override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(true)
          navigationView.isHidden = true
     }
     
     fileprivate func initViewModel() {
      
          viewModel.updateCalendar = { [weak self] in
               if self?.viewModel.calendarDictionary.count == 0 {
                    let vc = QuestionPageAdultViewController.instantiate()
                    vc.subUserGuid = self?.userDetails?.subUserGuid ?? ""
                    self?.navigationController?.pushViewController(vc, animated: true)
               } else {
                         self?.getStatistics()
                         self?.collectionView.reloadData()
               }
          }
          
          viewModel.updateVaccines = { [weak self] in
               self?.collectionView.reloadData()
          }
     }
     
     private func collectionViewConfigure() {
          let layout = UPCarouselFlowLayout()
          layout.itemSize = CGSize(width: 205, height: 375)
          layout.scrollDirection = .horizontal
          layout.sideItemAlpha = 1.0
          layout.sideItemAlpha = 0.8
          layout.spacingMode = .fixed(spacing: 5.0)
          collectionView.collectionViewLayout = layout
          collectionView.delegate = self
          collectionView.dataSource = self
          collectionView.register(UINib(nibName: "VaccineCalendarCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "vaccineCell")
     }
     
     private func getUser() {
          let param = ["userGuid": KeychainService.getUserGUID() ?? ""] as [String : Any]
          UserService.getUser(param, vc: self) { (users, error) in
               self.subUserDetails = users
               self.subUserDetails = self.subUserDetails?.filter({ $0.subUserGuid == self.userDetails?.subUserGuid })
               self.updateNavigationBar()
               self.navigationView.isHidden = false
          }
     }
     
     private func getStatistics() {
          stackView.removeFullyAllArrangedSubviews()
          self.viewModel.calendarDictionaryKeys.forEach { (key) in
               var subViewArr = [UIView]()
               self.viewModel.calendarDictionary[key]?.forEach { (item) in
                    let sView = UIView()
                    if self.isAdult {
                         if item.completed ?? false{
                              sView.backgroundColor = UIColor(red: 120/255, green: 192/255, blue: 142/255, alpha: 1)
                         } else {
                              sView.backgroundColor = .white
                         }
                    } else {
                         if item.customVaccineDate != "" {
                              sView.backgroundColor = UIColor(red: 120/255, green: 192/255, blue: 142/255, alpha: 1)
                         } else {
                              sView.backgroundColor = .white
                         }
                    }
                    subViewArr.append(sView)
               }
               print(subViewArr.count)
               
               let secondStack = UIStackView(arrangedSubviews: subViewArr)
               secondStack.axis = .horizontal
               secondStack.distribution = .fillEqually
               secondStack.backgroundColor = .white
               secondStack.layer.borderWidth = 1
               secondStack.layer.borderColor = UIColor.gray.cgColor
               stackView.addArrangedSubview(secondStack)
          }
     }
     
     private func filterVaccines(value: [SubUserCalendarModel], index: Int) {
          self.viewModel.vaccinesArray?.forEach({ (vaccines) in
               
               if value[index].vaccineIdList?.count == 1 {
                    let hasElement = (value[index].vaccineIdList?.contains(vaccines.id))!
                    if hasElement {
                         hasVaccine = vaccines.description.htmlToString ?? ""
                    }
               } else {
                    hasVaccine = "Bu aşı türü \(value[index].vaccineIdList?.count ?? 0) adet aşı içermektedir."
               }
          })
     }
     
     private func vaccinatedRequest(dictValue: Int, index: Int) {
          let key = self.viewModel.calendarDictionaryKeys[dictValue]
          let values = self.viewModel.calendarDictionary[key]
          
          let param = ["userGuid": KeychainService.getUserGUID() ?? "",
                       "subUserGuid":userDetails?.subUserGuid ?? "",
                       "vaccineCalendarId": values?[index].id ?? "",
                       "customNote": "",
                       "customVaccineDate": values?[index].vaccineDate ?? "",
                       "completed": true
          ] as [String : Any]
          
          if isAdult {
               VaccineDetailService.vaccineAdultDetails(param, vc: self) { (checkResp, error) in
                    
                    if checkResp != nil
                    {
                         if !checkResp!.success {
                              print("Error")
                              self.alert(message: checkResp!.message)
                         }else{
                              self.viewModel.getAdultUserCalendar(subUserGuid: self.userDetails?.subUserGuid)
                              print(checkResp!.message)
                         }
                    }
               }
          } else {
               VaccineDetailService.vaccineChildDetails(param, vc: self) { [self] (checkResp, error) in
                    
                    if checkResp != nil
                    {
                         if !checkResp!.success {
                              print("Error")
                              self.alert(message: checkResp!.message)
                         }else{
                              self.viewModel.getChildUserCalendar(subUserGuid: self.userDetails?.subUserGuid)
                              print(checkResp!.message)
                         }
                    }
               }
          }
     }
     
     @IBAction func editButton(_ sender: Any) {
          if filterAge >= 18 {
               let vc = AddAdultViewController.instantiate()
               vc.subUserDetails = self.subUserDetails
               vc.isRegister = true
               self.show(vc, sender: nil)
          } else {
               let vc = AddChildViewController.instantiate()
               vc.subUserDetails = self.subUserDetails
               vc.isRegister = true
               self.show(vc, sender: nil)
          }
     }
}

extension VaccineCalendarViewController: UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return self.viewModel.calendarDictionary.count
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "vaccineCell", for: indexPath) as! VaccineCalendarCollectionViewCell
          cell.tableView.delegate = self
          cell.tableView.dataSource = self
          cell.tableView.tag = indexPath.row
          cell.tableView.reloadData()
          return cell
     }
}

extension VaccineCalendarViewController: UITableViewDelegate, UITableViewDataSource {
     
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return getVaccineListCount(tableView.tag)
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
          if indexPath.row == 0 {
               let cell = tableView.dequeueReusableCell(withIdentifier: "vaccineCellDate", for: indexPath) as! VaccineCalendarDateTableViewCell
               getSubTextList(tableView.tag, index: indexPath.row, cell: cell)
               cell.delegate = self
               return cell
          }
          
          let cell = tableView.dequeueReusableCell(withIdentifier: "vaccineCell", for: indexPath) as! VaccineCalendarTableViewCell
          cell.delegate = self
          cell.tag = tableView.tag
          cell.checkButton.tag = indexPath.row - 1
          cell.dotButton.tag = indexPath.row - 1
          getVaccineList(tableView.tag, index: indexPath.row - 1, cell: cell)
          return cell
     }
     
     //     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     //          if let cell = tableView.cellForRow(at: indexPath) as? VaccineCalendarTableViewCell {
     //               fromVaccineCalendarToDetails(index: indexPath.row - 1, value: tableView.tag, cell: cell)
     //          }
     //     }
     
     func getSubTextList(_ value:Int ,index:Int, cell:VaccineCalendarDateTableViewCell) {
          let key = self.viewModel.calendarDictionaryKeys[value]
          let values = self.viewModel.calendarDictionary[key]
          
          if isAdult {
               DispatchQueue.main.async {
                    cell.configureAdult(key: key, values: values ?? [], index: index,birthDate: self.subUserDetails?.first?.birthDate ?? "")
               }
          } else {
               cell.configureChild(key: key, values: values ?? [], index: index)
          }
     }
     
     func getVaccineList(_ value:Int ,index:Int, cell:VaccineCalendarTableViewCell) {
          let key = self.viewModel.calendarDictionaryKeys[value]
          let values = self.viewModel.calendarDictionary[key]
          if isAdult {
               cell.configureAdult(key: key, values: values ?? [], index: index)
          } else {
               cell.configureChild(key: key, values: values ?? [], index: index)
               self.filterVaccines(value: values ?? [], index: index)
               cell.vaccineDescription.text = hasVaccine
          }
     }
     
     func getVaccineListCount(_ value:Int) -> Int {
          let key = self.viewModel.calendarDictionaryKeys[value]
          return self.viewModel.calendarDictionary[key]!.count + 1
     }
     
     //     func fromVaccineCalendarToDetails(index: Int, value: Int, cell:VaccineCalendarTableViewCell) {
     //          let vc = VaccineDetailViewController.instantiate()
     //          let values = Array(self.viewModel.calendarDictionary.values)[value]
     //
     //          vc.vaccineDesc = cell.vaccineDescription.text ?? ""
     //          vc.vaccineDetailList = values[index]
     //          vc.userDetails = self.userDetails
     //          vc.vaccines = self.viewModel.vaccinesArray
     //          vc.isAdult = self.isAdult
     //          self.show(vc, sender: nil)
     //     }
}

extension VaccineCalendarViewController: VaccineCalendarDateTableViewCellProtocol {
     func addVaccineBtn() {
          let vc = NewVaccineViewController.instantiate()
          self.show(vc, sender: nil)
     }
}

extension VaccineCalendarViewController: VaccineCalendarTableViewCellProtocol {
     func dotButton(index: Int, value: Int, desc: String) {
          let vc = VaccineDetailViewController.instantiate()
          
          let key = self.viewModel.calendarDictionaryKeys[value]
          let values = self.viewModel.calendarDictionary[key]
          
          //        let values = Array(self.viewModel.calendarDictionary.values)[value]
          
          vc.vaccineDesc = desc
          vc.vaccineDetailList = values![index]
          vc.userDetails = self.userDetails
          vc.vaccines = self.viewModel.vaccinesArray
          vc.isAdult = self.isAdult
          self.show(vc, sender: nil)
     }
     
     func checkButton(index: Int, value: Int) {
          vaccinatedRequest(dictValue: value, index: index)
     }
}

extension VaccineCalendarViewController: StoryboardInstatiate, Reusable {
     public static var storyboard: Storyboard { return .vaccine }
}

