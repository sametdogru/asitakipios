//
//  CollectiveVaccineDetailsViewController.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 27.10.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class CollectiveVaccineDetailsViewController: BaseVC {
    @IBOutlet weak var tableView: UITableView!

    var vaccineIdList: Vaccines?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
       
    }
    
    func initUI() {
        title = "Aşı Detayı"
        self.leftNavigationItem = .back
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
}

extension CollectiveVaccineDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CollectiveVaccineDetailsTableViewCell
        cell.vaccineNameLbl.text = vaccineIdList?.name
        cell.vaccineDescriptionLbl.text = vaccineIdList?.description.htmlToString
        return cell
    }
    
}

extension CollectiveVaccineDetailsViewController: StoryboardInstatiate, Reusable {
     public static var storyboard: Storyboard { return .vaccine }
}
