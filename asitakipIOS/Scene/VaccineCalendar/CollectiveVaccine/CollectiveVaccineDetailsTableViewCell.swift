//
//  CollectiveVaccineDetailsTableViewCell.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 13.11.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class CollectiveVaccineDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var vaccineNameLbl: UILabel!
    @IBOutlet weak var vaccineDescriptionLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
