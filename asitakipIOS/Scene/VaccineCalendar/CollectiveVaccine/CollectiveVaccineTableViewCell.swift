//
//  CollectiveVaccineTableViewCell.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 27.10.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class CollectiveVaccineTableViewCell: UITableViewCell {

    @IBOutlet weak var vaccineName: UILabel!
    @IBOutlet weak var vaccineDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
