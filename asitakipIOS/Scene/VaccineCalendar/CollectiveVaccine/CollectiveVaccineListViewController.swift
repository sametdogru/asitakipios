//
//  CollectiveVaccineListViewController.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 27.10.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class CollectiveVaccineListViewController: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.leftNavigationItem = .back
    }
    
    func tableViewConfigure() {
        tableView.delegate = self
        tableView.dataSource = self
    }
}


extension CollectiveVaccineListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        <#code#>
    }
    
    
}

extension CollectiveVaccineListViewController: StoryboardInstatiate, Reusable {
     public static var storyboard: Storyboard { return .main }
}
