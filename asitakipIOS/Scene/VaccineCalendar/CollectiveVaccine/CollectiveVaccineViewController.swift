//
//  CollectiveVaccineListViewController.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 27.10.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class CollectiveVaccineListViewController: BaseVC {

    @IBOutlet weak var tableView: UITableView!
    var vaccineIdList = [Vaccines]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.leftNavigationItem = .back
        tableViewConfigure()
    }
    
    func tableViewConfigure() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.reloadData()
    }
}

extension CollectiveVaccineListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vaccineIdList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CollectiveVaccineTableViewCell
        cell.vaccineName.text = vaccineIdList[indexPath.row].name
        cell.vaccineDescription.text = vaccineIdList[indexPath.row].description.htmlToString
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = CollectiveVaccineDetailsViewController.instantiate()
        vc.vaccineIdList = vaccineIdList[indexPath.row]
        self.show(vc, sender: nil)
    }
}

extension CollectiveVaccineListViewController: StoryboardInstatiate, Reusable {
     public static var storyboard: Storyboard { return .vaccine }
}
