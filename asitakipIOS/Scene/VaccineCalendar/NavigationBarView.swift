//
//  Ex.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 18.11.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import UIKit

extension VaccineCalendarViewController {
     
     public func updateNavigationBar() {
          
          let dateFormatter = DateFormatter()
          dateFormatter.locale = .init(identifier: "tr_TR")
          dateFormatter.dateFormat = "yyyy-MM-dd"
          
          let day = dateFormatter.date(from: subUserDetails?.first?.birthDate ?? "")
          let month = dateFormatter.date(from: subUserDetails?.first?.birthDate ?? "")
          let year = dateFormatter.date(from: subUserDetails?.first?.birthDate ?? "")
          
          let dayComponent = Calendar.current.dateComponents([.day], from: day ?? Date(), to: Date())
          let monthComponent = Calendar.current.dateComponents([.month], from: month ?? Date(), to: Date())
          let yearComponent = Calendar.current.dateComponents([.year], from: year ?? Date(), to: Date())
          
          let days = dayComponent.day ?? 0
          let monthly = monthComponent.month ?? 0
          let yearly = yearComponent.year ?? 0
          self.filterAge = yearly
          
          if days < 30 {
               self.age = "\(days) Günlük"
          } else if days >= 30 && days < 365 {
               self.age = "\(monthly) Aylık"
          } else if days > 365 {
               self.age = "\(yearly) Yaşında"
          }
          
          self.initials = "\(subUserDetails?.first?.name?.initials ?? "")\(subUserDetails?.first?.surname?.initials ?? "")"
          navigationController?.navigationBar.addSubview(setNavigationBar(initials: initials, name: "\(subUserDetails?.first?.name ?? "") \(subUserDetails?.first?.surname ?? "")", age: "\(age)"))
     }
     
     public func setNavigationBar(initials: String, name:String, age:String) -> UIView {
          
          let initialLabel = UILabel(frame: CGRect(x: 5, y: 5, width: 30, height: 30))
          initialLabel.textColor = .white
          initialLabel.font = UIFont.boldSystemFont(ofSize: 16)
          initialLabel.text = initials
          initialLabel.clipsToBounds = true
          initialLabel.textAlignment = .center
          initialLabel.backgroundColor = .lightGray
          initialLabel.layer.cornerRadius = 15
          
          let nameLabel = UILabel(frame: CGRect(x: 42, y: 5, width: 0, height: 0))
          
          nameLabel.backgroundColor = UIColor.clear
          nameLabel.textColor = UIColor.black
          nameLabel.font = UIFont.boldSystemFont(ofSize: 16)
          nameLabel.textAlignment = .left
          nameLabel.text = name
          nameLabel.sizeToFit()
          
          let ageLabel = UILabel(frame: CGRect(x:42, y:25, width:0, height:0))
          ageLabel.backgroundColor = .clear
          ageLabel.textColor = .gray
          ageLabel.font = UIFont.systemFont(ofSize: 12)
          ageLabel.textAlignment = .left
          ageLabel.text = age
          ageLabel.sizeToFit()
          
          let titleView = UIView(frame: CGRect(x: 60, y: 0, width: max(initialLabel.frame.size.width, nameLabel.frame.size.width, ageLabel.frame.size.width), height: 30))
          titleView.addSubview(initialLabel)
          titleView.addSubview(nameLabel)
          titleView.addSubview(ageLabel)
          navigationView = titleView
          
          return titleView
     }
}
