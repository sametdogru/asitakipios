//
//  SubUserCalendarModel.swift
//  asitakipIOS
//
//  Created by Mac on 5.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import SwiftyJSON


struct SubUserCalendarModel: Codable,JSONCollectionSerializable {
    
    var vaccineGroupName:String?
    var valueType:String?
    var text: String?
    var completed: Bool?
    var customVaccineDate:String?
    var customNote:String?
    var subText: String?
    var vaccineIdList: [Int]?
    var addedValue: Int?
    var dose: String?
    var vaccineDate: String?
    var id: String?
    
    init(json: JSON) {
        text = json["text"].stringValue
        completed = json["completed"].boolValue
        vaccineGroupName = json["vaccineGroupName"].stringValue
        valueType = json["valueType"].stringValue
        customVaccineDate = json["customVaccineDate"].stringValue
        customNote = json["customNote"].stringValue
        subText = json["subText"].stringValue
        vaccineIdList = json["vaccineIdList"].arrayObject as? [Int]
        addedValue = json["addedValue"].intValue
        dose = json["dose"].stringValue
        vaccineDate = json["vaccineDate"].stringValue
        id = json["id"].stringValue
    }
}
