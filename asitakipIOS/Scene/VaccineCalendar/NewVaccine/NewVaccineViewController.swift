//
//  NewVaccineViewController.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 5.11.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class NewVaccineViewController: BaseVC {

    @IBOutlet weak var dateLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    func initUI() {
        
        title = "Yeni Aşı"
        self.leftNavigationItem = .back
        
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let time = formatter.string(from: currentDateTime)
        dateLbl.text = time
    }
    
    @IBAction func editDateButton(_ sender: Any) {
        let vc = DatePickerController.instantiate()
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func saveButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        print("save button tapped")
    }
}

extension NewVaccineViewController: DatePickerProtocol {
    func selectedDate(date: String) {
        self.dateLbl.text = date
    }
}

extension NewVaccineViewController: StoryboardInstatiate, Reusable {
     public static var storyboard: Storyboard { return .vaccine }
}
