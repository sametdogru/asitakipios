//
//  VaccineCalendarViewModel.swift
//  asitakipIOS
//
//  Created by Mac on 18.11.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//


import Foundation
import UIKit

class VaccineCalendarViewModel {
    
    
    var subUserCalendar: [SubUserCalendarModel]?
    
    var calendarDictionaryKeys = [String]()
    
    var calendarDictionary = [String: [SubUserCalendarModel]]()
    
    var vaccinesArray: [Vaccines]?
    
    var updateVaccines: ()->() = {}

    var updateCalendar: ()->() = {}

    public func getChildUserCalendar(subUserGuid: String?) {
        let param = ["userGuid": KeychainService.getUserGUID() ?? "",
                     "subUserGuid":subUserGuid ?? ""] as [String : Any]
        self.calendarDictionary.removeAll()
        self.calendarDictionaryKeys.removeAll()
        
        SubUserCalendarService.subUserCalendar(param, vc: nil) { [self] (model, error) in
            self.subUserCalendar = model
            
            for item in self.subUserCalendar! {
                if var mapItem = self.calendarDictionary[item.vaccineDate ?? ""], mapItem.count > 0 {
                    mapItem.append(item)
                    self.calendarDictionary[item.vaccineDate ?? ""] = mapItem
                } else {
                    var newList = [SubUserCalendarModel]()
                    newList.append(item)
                    self.calendarDictionary[item.vaccineDate ?? ""] = newList
                }
            }
            self.calendarDictionaryKeys = Array(calendarDictionary.keys).sorted(by: <)
            DispatchQueue.main.async {
                self.updateCalendar()
            }
        }
    }
    
    public func getAdultUserCalendar(subUserGuid: String?) {
        
        let param = ["userGuid": KeychainService.getUserGUID() ?? "",
                     "subUserGuid":subUserGuid ?? ""] as [String : Any]
        
        self.calendarDictionary.removeAll()
        self.calendarDictionaryKeys.removeAll()

        SubUserAdultCalendarService.subUserAdultCalendar(param, vc: nil) { [self] (model, error) in
            self.subUserCalendar = model
            
            for item in self.subUserCalendar! {
                if var mapItem = self.calendarDictionary[item.vaccineDate ?? ""], mapItem.count > 0 {
                    mapItem.append(item)
                    self.calendarDictionary[item.vaccineDate ?? ""] = mapItem
                    
                } else {
                    var newList = [SubUserCalendarModel]()
                    newList.append(item)
                    self.calendarDictionary[item.vaccineDate ?? ""] = newList
                }
            }
            self.calendarDictionaryKeys = Array(calendarDictionary.keys).sorted(by: <)
            self.updateCalendar()
            print(calendarDictionary.count)
            
        }
    }
    
    public func getVaccines() {
        VaccinesService.getVaccines { (vaccines, error) in
            self.vaccinesArray = vaccines
            self.updateVaccines()
        }
    }
}


