//
//  BeforeTravelViewController.swift
//  asitakipIOS
//
//  Created by Mac on 1.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class BeforeTravelViewController: BaseVC {
    
    @IBOutlet weak var countryText: UITextField!
    @IBOutlet weak var vaccineDetailText: UITextView!
    
    private var countries:[Countries]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = false
        
        title = "Seyehat Öncesi"
        self.leftNavigationItem = .back
        getCities()
       
    }
    
    func getCities() {
        CountryService.getCountry {[weak self] (cities, error) in
            self?.countries = cities
            self?.countryText.text = self?.countries?[0].name
            self?.vaccineDetailText.text = self?.countries?[0].description.htmlToString
        }
    }
    
    @IBAction func selectCity(_ sender: UITextField) {
        sender.resignFirstResponder()
        let vc = CountryPickerController.instantiate()
        vc.countryPickerDelegate = self
        vc.countryDetailsDelegate = self
        self.present(vc, animated: true, completion: nil)
    }
}

extension BeforeTravelViewController: CountryPickerProtocol {
    func selectedCountry(country: String) {
        self.countryText.text = country
    }
}

extension BeforeTravelViewController: CountryDetailsProtocol {
    func countryDetails(description: String) {
        self.vaccineDetailText.text = description.htmlToString ?? ""
        print(description.htmlToString ?? "")
    }
}

extension BeforeTravelViewController: StoryboardInstatiate,Reusable {
    public static var storyboard: Storyboard { return .travel }
}


extension String {
   public var htmlToString: String? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil).string
        } catch let error as NSError {
            print(error.localizedDescription)
            return nil
        }
    }
}
