//
//  SecondQuestionViewController.swift
//  asitakipIOS
//
//  Created by Mac on 15.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class TraumaSecondQuestionViewController: BaseVC {

    @IBOutlet weak var yesRadioButton: UIButton!
    @IBOutlet weak var noRadioButton: UIButton!
    
    @IBOutlet weak var yesRadioClick: UIButton!
    @IBOutlet weak var noRadioClick: UIButton!
    
    var isRabies: Bool = false
    var isProvocation: Bool = false
    
    var firstClick: Bool = false
    var secondClick: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.leftNavigationItem = .back
        title = "Travma Sonrası"
    }
    
    @IBAction func yesRadioButton(_ sender: Any) {
        if yesRadioButton.isSelected == false {
            firstClick = true
            yesRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            noRadioButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
        }
    }
    
    @IBAction func noRadioButton(_ sender: Any) {
        if noRadioButton.isSelected == false {
            firstClick = true
            noRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            yesRadioButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
        }
    }
    
    @IBAction func yesRadioClick(_ sender: Any) {
        if yesRadioClick.isSelected == false {
            secondClick = true
            yesRadioClick.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            noRadioClick.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
        }
    }
    
    @IBAction func noRadioClick(_ sender: Any) {
        if noRadioClick.isSelected == false {
            secondClick = true
            noRadioClick.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            yesRadioClick.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
        }
    }
    
    @IBAction func okButton(_ sender: Any) {
        if firstClick == false || secondClick == false {
            self.alert(message: "Lütfen boş alan bırakmayınız.")
        } else {
            let vc = TraumaThirdQuestionViewController.instantiate()
            vc.isRabies = self.isRabies
            vc.isProvocation = self.isProvocation
            self.show(vc, sender: nil)
        }
    }
}

extension TraumaSecondQuestionViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .trauma }
}
