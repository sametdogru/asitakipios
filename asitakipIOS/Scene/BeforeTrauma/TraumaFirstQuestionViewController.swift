//
//  FirstQuestionViewController.swift
//  asitakipIOS
//
//  Created by Mac on 15.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class TraumaFirstQuestionViewController: BaseVC {

    @IBOutlet weak var animalText: UITextField!
    @IBOutlet weak var spontaneousRadioButton: UIButton!
    @IBOutlet weak var provocationRadioButton: UIButton!
    
    var isRabies: Bool = false
    var isProvocation: Bool? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.leftNavigationItem = .back
        title = "Travma Sonrası"
    }
    
    @IBAction func selectAnimal(_ sender: UITextField) {
        sender.resignFirstResponder()
        let vc = AnimalsViewController.instantiate()
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func spontaneousRadioButton(_ sender: Any) {
        if spontaneousRadioButton.isSelected == false {
            self.isProvocation = true
            spontaneousRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            provocationRadioButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
        }
    }
    
    @IBAction func provocationRadioButton(_ sender: Any) {
        if provocationRadioButton.isSelected == false {
            self.isProvocation = false
            provocationRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            spontaneousRadioButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
        }
    }
    
    @IBAction func okButton(_ sender: Any) {
        if animalText.text == "" || self.isProvocation == nil {
            self.alert(message: "Lütfen boş alan bırakmayınız.")
        } else {
            let vc = TraumaSecondQuestionViewController.instantiate()
            vc.isRabies = self.isRabies
            vc.isProvocation = self.isProvocation ?? false
            self.show(vc, sender: nil)
        }
    }
}

extension TraumaFirstQuestionViewController: AnimalPickerProtocol {
    func selectedAnimal(animal: String, rabies: Bool) {
        self.animalText.text = animal
        self.isRabies = rabies
    }
}

extension TraumaFirstQuestionViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .trauma }
}
