//
//  ThirdQuestionViewController.swift
//  asitakipIOS
//
//  Created by Mac on 15.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class TraumaThirdQuestionViewController: BaseVC {
    
    var isRabies: Bool = false
    var isProvocation: Bool = true
    var isHasOwner: Bool = true
    var isHasVaccine: Bool = true

    var firstClick: Bool = false
    var secondClick: Bool = false
    
    @IBOutlet weak var animalOwner: UIButton!
    @IBOutlet weak var notAnimalOwner: UIButton!
    @IBOutlet weak var vaccinatedAnimal: UIButton!
    @IBOutlet weak var notVaccinatedAnimal: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.leftNavigationItem = .back
        title = "Travma Sonrası"
    }
    
    @IBAction func animalOwnerRadioButton(_ sender: Any) {
        if animalOwner.isSelected == false {
            self.firstClick = true
            self.isHasOwner = true
            animalOwner.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            notAnimalOwner.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
        }
    }
    
    @IBAction func notAnimalOwnerRadioButton(_ sender: Any) {
        if notAnimalOwner.isSelected == false {
            self.firstClick = true
            self.isHasOwner = false
            notAnimalOwner.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            animalOwner.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
        }
    }
    
    @IBAction func vaccinatedAnimalRadioButton(_ sender: Any) {
        if vaccinatedAnimal.isSelected == false {
            self.secondClick = true
            self.isHasVaccine = true
            vaccinatedAnimal.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            notVaccinatedAnimal.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
        }
    }
    
    @IBAction func notVaccinatedAnimalRadioButton(_ sender: Any) {
        if notVaccinatedAnimal.isSelected == false {
            self.secondClick = true
            self.isHasVaccine = false
            notVaccinatedAnimal.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            vaccinatedAnimal.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
        }
    }
    
    @IBAction func okButton(_ sender: Any) {
        if firstClick == false || secondClick == false {
            self.alert(message: "Lütfen boş alan bırakmayınız.")
        } else {
            if isRabies == true {
                if !isHasOwner {
                    let vc = HighViewController.instantiate()
                    vc.nav = self.navigationController
                    self.present(vc, animated: true, completion: nil)
                }
                
                if !isHasVaccine {
                    let vc = HighViewController.instantiate()
                    vc.nav = self.navigationController
                    self.present(vc, animated: true, completion: nil)
                }
                
                if !isProvocation {
                    let vc = HighViewController.instantiate()
                    vc.nav = self.navigationController
                    self.present(vc, animated: true, completion: nil)
                }
                
                let vc = NormalViewController.instantiate()
                vc.nav = self.navigationController
                self.present(vc, animated: true, completion: nil)
                
            } else {
                let vc = LowViewController.instantiate()
                vc.nav = self.navigationController
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
}

extension TraumaThirdQuestionViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .trauma }
}
