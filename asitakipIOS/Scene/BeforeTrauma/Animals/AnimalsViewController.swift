//
//  AnimalsViewController.swift
//  asitakipIOS
//
//  Created by Mac on 15.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

protocol AnimalPickerProtocol {
    func selectedAnimal(animal:String, rabies:Bool)
}

struct AnimalModel {
   let name: String
   let rabies: Bool
}

class AnimalsViewController: UIViewController {

    var delegate: AnimalPickerProtocol?
    var animals: [AnimalModel] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getAnimals()
        tableViewConfigure()
        tableView.reloadData()
    }
    
    func getAnimals() {
        var its = [AnimalModel]()
        its.append(AnimalModel(name: "Köpek", rabies: true))
        its.append(AnimalModel(name: "Kedi", rabies: true))
        its.append(AnimalModel(name: "Sığır", rabies: true))
        its.append(AnimalModel(name: "Porsuk", rabies: true))
        its.append(AnimalModel(name: "Kurt", rabies: true))
        its.append(AnimalModel(name: "Koyun", rabies: true))
        its.append(AnimalModel(name: "Eşek", rabies: true))
        its.append(AnimalModel(name: "Tilki", rabies: true))
        its.append(AnimalModel(name: "Fare", rabies: false))
        its.append(AnimalModel(name: "Sıçan", rabies: false))
        its.append(AnimalModel(name: "Sincap", rabies: false))
        its.append(AnimalModel(name: "Hamster", rabies: false))
        its.append(AnimalModel(name: "Kobay", rabies: false))
        its.append(AnimalModel(name: "Tavşan", rabies: false))
        its.append(AnimalModel(name: "Kümes Hayvanı", rabies: false))
        its.append(AnimalModel(name: "Yılan", rabies: false))
        its.append(AnimalModel(name: "Kertenkele", rabies: false))
        its.append(AnimalModel(name: "Kaplumbağa", rabies: false))
        
        animals = its
    }
    
    func tableViewConfigure() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "AnimalsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }
}

extension AnimalsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return animals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AnimalsTableViewCell
        cell.animalLbl.text = animals[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.selectedAnimal(animal: animals[indexPath.row].name, rabies: animals[indexPath.row].rabies)
        self.dismiss(animated: true, completion: nil)
    }
}

extension AnimalsViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .trauma }
}
