//
//  AnimalsTableViewCell.swift
//  asitakipIOS
//
//  Created by Mac on 15.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class AnimalsTableViewCell: UITableViewCell {

    @IBOutlet weak var animalLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
