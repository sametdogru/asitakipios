//
//  HighViewController.swift
//  asitakipIOS
//
//  Created by Mac on 15.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class HighViewController: BaseVC {

    var nav: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func okButton(_ sender: Any) {
        
        self.dismiss(animated: true) {
            let vc = HomeViewController.instantiate()
            self.nav?.pushViewController(vc, animated: true)
        }
    }
}

extension HighViewController: StoryboardInstatiate,Reusable {
    public static var storyboard: Storyboard { return .trauma }
}
