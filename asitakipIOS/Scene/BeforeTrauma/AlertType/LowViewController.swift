//
//  LowViewController.swift
//  FacebookCore
//
//  Created by Mac on 15.05.2020.
//

import UIKit

class LowViewController: UIViewController {

    var nav: UINavigationController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func okButton(_ sender: Any) {
         self.dismiss(animated: true) {
            let vc = HomeViewController.instantiate()
            self.nav?.pushViewController(vc, animated: true)
               }
    }
}

extension LowViewController: StoryboardInstatiate,Reusable {
    public static var storyboard: Storyboard { return .trauma }
}
