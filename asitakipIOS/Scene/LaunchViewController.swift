//
//  LaunchViewController.swift
//  asitakipIOS
//
//  Created by Samet Dogru on 8.11.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true

        let isActive = UserDefaults.standard.bool(forKey: userActive)
        if !isActive {
            KeychainService.deleteKey(userGuid)
            KeychainService.deleteKey(activation)
        }
        let activationCompleted =  KeychainService.isActivationCompleted()
        if let key = KeychainService.getUserGUID(), activationCompleted, !key.hasPrefix("0000") {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                let vc = HomeViewController.instantiate()
                self.navigationController?.pushViewController(vc, animated: true)
             }
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.gotoLogin()
             }
        }
    }
    
    func gotoLogin(){
        if  OnBoardingVC.alreadyShowed() {
            let vc = MainViewController.instantiate()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = OnBoardingVC.instantiate()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    public func openViewController(with storyBoardName: String, viewController: String, option: UIView.AnimationOptions) {
         DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
             guard let window = UIApplication.shared.keyWindow else {
                 return
             }
             
             let storyboard = UIStoryboard(name: storyBoardName, bundle: nil)
             let vc = storyboard.instantiateViewController(withIdentifier: viewController)
             let navc = UINavigationController(rootViewController: vc)
             
             UIApplication.shared.delegate?.window??.rootViewController = navc
             window.rootViewController = navc
             
             let options: UIView.AnimationOptions = option
             let duration: TimeInterval = 0.3
             UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
                 { completed in
                     
             })
         }
     }
}


extension LaunchViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { .main }
}
