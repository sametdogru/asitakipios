//
//  VaccineDetailModel.swift
//  asitakipIOS
//
//  Created by Mac on 7.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import SwiftyJSON


struct VaccineDetailModel: Codable,JSONCollectionSerializable {
    
    var vaccineGroupName:String?
    var valueType:String?
    var customVaccineDate:String?
    var customNote:String?
    var subText: String?
    var vaccineIdList: [Int]?
    var addedValue: Int?
    var dose: String?
    var vaccineDate: String?
    var id: String?
    
    
    init(json: JSON) {
        vaccineGroupName = json["vaccineGroupName"].stringValue
        valueType = json["valueType"].stringValue
        customVaccineDate = json["customVaccineDate"].stringValue
        customNote = json["customNote"].stringValue
        subText = json["subText"].stringValue
        vaccineIdList = json["professionId"].arrayObject as? [Int]
        addedValue = json["diseases"].intValue
        dose = json["dose"].stringValue
        vaccineDate = json["vaccineDate"].stringValue
        id = json["id"].stringValue
    }
}
