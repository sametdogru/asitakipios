//
//  VaccineDetailService.swift
//  asitakipIOS
//
//  Created by Mac on 7.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation

struct VaccineDetailService {
    static func vaccineChildDetails(_ parameter:[String:Any],vc:BaseVC? = nil,completionHandler: @escaping ( _ respone:CheckResponse?, _ error:MyError?) -> Swift.Void){
        let urlStr = String(format: Service.SubUserVaccineCalendar, parameter)
        Network.shared.post(url: urlStr,parameters: parameter, showIndicator:true) {  (response,error) in
            
            var checkRes:CheckResponse?
             
            if error == nil
            {
            checkRes = CheckResponse(json: response)
            }
        
         if !RegisterSubUserService.showError(vc, checkRes, error) {
                 completionHandler(checkRes,error)
            }
        }
    }
    
    static func vaccineAdultDetails(_ parameter:[String:Any],vc:BaseVC? = nil,completionHandler: @escaping ( _ respone:CheckResponse?, _ error:MyError?) -> Swift.Void){
        let urlStr = String(format: Service.SubUserAdultVaccineCalendar, parameter)
        Network.shared.post(url: urlStr,parameters: parameter, showIndicator:true) {  (response,error) in
            
            var checkRes:CheckResponse?
             
            if error == nil
            {
            checkRes = CheckResponse(json: response)
            }
        
         if !RegisterSubUserService.showError(vc, checkRes, error) {
                 completionHandler(checkRes,error)
            }
        }
    }
    
    static func getVaccines(_ completionHandler: @escaping (_ cities: [Vaccines]?, _ error:MyError?) -> Swift.Void){
         let urlStr = String(describing: Service.Vaccines)
         Network.shared.get(url: urlStr) { (response, error) in
             
             if error == nil {
                 print(response)
    completionHandler(Vaccines.getCollection(json: response["data"]),nil)
             } else {
                 completionHandler(nil,error)
             }
         }
     }
}

