//
//  VaccineDetailViewController.swift
//  asitakipIOS
//
//  Created by Mac on 10.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit
import ReadMoreTextView

class VaccineDetailViewController: BaseVC {
    
    @IBOutlet weak var vaccineDose: UILabel!
    @IBOutlet weak var vaccineType: UILabel!
    @IBOutlet weak var dateText: UILabel!
    @IBOutlet weak var noteText: UITextView!
    @IBOutlet weak var vaccineDescriptionView: UIView!
    @IBOutlet weak var vaccineDescription: ReadMoreTextView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var vaccines: [Vaccines]?
    var vaccineDetailList:SubUserCalendarModel?
    var userDetails: FilterUserModel?
    var vaccineDate = String()
    var customVaccineDate = String()
    var vaccineDesc = String()
    
    var isAdult: Bool = false
    var vaccineIdList = [Vaccines]()

    override func viewDidLoad() {
        super.viewDidLoad()
            initUI()
    }
    
    func initUI() {
        title = "Aşı Detayı"
        self.leftNavigationItem = .back
        self.hideKeyboardWhenTappedAround()
        
        vaccineDose.text = "\(vaccineDetailList?.dose ?? "") Doz"
        vaccineType.text = vaccineDetailList?.vaccineGroupName
        noteText.text = vaccineDetailList?.customNote
        
        if vaccineDetailList?.vaccineIdList?.count ?? 0 > 1 {
            vaccineDescription.text = "\(vaccineDesc) Detayları görmek için tıklayınız."
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(todetails))
            vaccineDescriptionView.isUserInteractionEnabled = true
            vaccineDescriptionView.addGestureRecognizer(recognizer)
        } else {
            vaccineDescription.text = "\(vaccineDesc)"
        }

        if vaccineDetailList?.customVaccineDate?.isEmpty == false {
            vaccineDateformatter(date: vaccineDetailList?.customVaccineDate ?? "")
            dateText.text = vaccineDate
        } else {
            vaccineDateformatter(date: vaccineDetailList?.vaccineDate ?? "")
            dateText.text = vaccineDate
        }
        
        let readMoreTextAttributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.foregroundColor:UIColor.systemBlue,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)
        ]
        let readLessTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.systemBlue,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)
        ]

        vaccineDescription.attributedReadMoreText = NSAttributedString(string: "... Daha fazla göster", attributes: readMoreTextAttributes)
        vaccineDescription.attributedReadLessText = NSAttributedString(string: " Daha az göster", attributes: readLessTextAttributes)
        vaccineDescription.maximumNumberOfLines = 6
        vaccineDescription.shouldTrim = true
        self.scrollView.contentSize = self.view.frame.size
        filterVaccines()
    }
    
    @objc func todetails() {
        let vc = CollectiveVaccineListViewController.instantiate()
        vc.vaccineIdList = self.vaccineIdList
        self.show(vc, sender: nil)
    }
    
    func filterVaccines() {
            self.vaccines?.forEach({ (vaccines) in
            
            let hasElement = self.vaccineDetailList?.vaccineIdList?.contains(vaccines.id)
            if hasElement ?? false {
                self.vaccineIdList.append(vaccines)
            }
         })
    }
    
    func vaccineDateformatter(date:String)  {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let vaccinedate = dateFormatter.date(from: date)
        let vaccineDateFormatter = DateFormatter()
        vaccineDateFormatter.dateFormat = "dd.MM.yyyy"
        vaccineDate = vaccineDateFormatter.string(from: vaccinedate!)
    }
    
    @IBAction func editDateButton(_ sender: Any) {
        let vc = DatePickerController.instantiate()
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func formatter(date:String) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        if let vaccinedate = dateFormatter.date(from: date) {
            let vaccineDateFormatter = DateFormatter()
            vaccineDateFormatter.dateFormat = "yyyy-MM-dd"
            self.vaccineDate = vaccineDateFormatter.string(from: vaccinedate )
        }
    }
    
    @IBAction func saveButton(_ sender: Any) {
        
        if vaccineDate.isEmpty == false {
            formatter(date: vaccineDate)
        }
        
        let param = ["userGuid": KeychainService.getUserGUID() ?? "",
                     "subUserGuid":userDetails?.subUserGuid ?? "",
                     "vaccineCalendarId": vaccineDetailList?.id ?? "",
                     "customNote": noteText.text ?? "",
                     "customVaccineDate": vaccineDate,
                     "completed": true
            ] as [String : Any]
        
        if isAdult {
            VaccineDetailService.vaccineAdultDetails(param, vc: self) { (checkResp, error) in
                
                if checkResp != nil
                {
                    if !checkResp!.success {
                        print("Error")
                        self.alert(message: checkResp!.message)
                    }else{
                        self.navigationController?.popViewController(animated: true)
                        print(checkResp!.message)
                    }
                }
            }
        } else {
            VaccineDetailService.vaccineChildDetails(param, vc: self) { (checkResp, error) in
                
                if checkResp != nil
                {
                    if !checkResp!.success {
                        print("Error")
                        self.alert(message: checkResp!.message)
                    }else{
                        self.navigationController?.popViewController(animated: true)
                        print(checkResp!.message)
                    }
                }
            }
        }
    }
}

extension VaccineDetailViewController {
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: {_ in
            self.vaccineDescription.setNeedsUpdateTrim()
        }, completion: nil)
    }
}

extension VaccineDetailViewController: DatePickerProtocol {
    func selectedDate(date: String) {
        formatter(date: date)
        self.dateText.text = date
    }
}

extension VaccineDetailViewController: StoryboardInstatiate,Reusable {
    public static var storyboard: Storyboard { return .vaccine }
}
