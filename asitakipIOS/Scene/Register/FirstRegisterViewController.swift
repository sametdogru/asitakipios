//
//  FirstRegisterViewController.swift
//  asitakipIOS
//
//  Created by Mac on 26.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class FirstRegisterViewController: BaseVC {
    
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var emailAlertLbl: UILabel!
    @IBOutlet weak var emailAlertIcon: UIImageView!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var confirmPasswordText: UITextField!
    @IBOutlet weak var nextButton: Button!
    @IBOutlet weak var showHideButton: UIButton!
    @IBOutlet weak var showHideClick: UIButton!
    
    var iconClick: Bool = true
    var secondIconClick: Bool = true
    var user:User = User()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    func initUI() {
        title = "Kayıt"
        self.leftNavigationItem = .back
        nextButton.makeActive()
        confirmView.isHidden = true
        errorView.isHidden = true
        self.hideKeyboardWhenTappedAround()
                
    }
    
    @IBAction func checkEmail(_ sender: UITextField) {
        confirmView.isHidden = false
        let email = isValidEmail(testStr: emailText.text!)
        
        if email == false {
            self.emailAlertIcon.image = #imageLiteral(resourceName: "error")
            self.emailAlertLbl.text = "Geçersiz e-posta adresi."
            
        } else {
            self.emailAlertIcon.image = #imageLiteral(resourceName: "ok")
            self.emailAlertLbl.text = "Geçerli e-posta adresi."
        }
    }
    
    
    @IBAction func checkPassword(_ sender: UITextField) {
        error()
    }
    
    @IBAction func checkConfirmPassword(_ sender: UITextField) {
        error()
    }
    
    func error() {
        if passwordText.text != confirmPasswordText.text {
            errorView.isHidden = false
        } else {
            errorView.isHidden = true
        }
    }
    
    @IBAction func nextButton(_ sender: Any) {
        if errorView.isHidden == false || passwordText.text == "" {
            showMessage(title: "Uyarı!", message: "Şifreler uyuşmuyor.")
        } else if self.emailAlertIcon.image == #imageLiteral(resourceName: "error") || self.emailText.text == "" {
            showMessage(title: "Uyarı!", message: "Geçerli e-posta giriniz.")
        } else if (self.passwordText.text?.count ?? 0) < 6 {
            showMessage(title: "Uyarı!", message: "Şifre en az 6 karakter içermelidir.")
        } else {
            user.email = emailText.text
            user.pwd = passwordText.text
            user.city = "Ankara"
            
            let vc = SecondRegisterViewController.instantiate()
            vc.user = self.user
            self.show(vc, sender: nil)
        }
    }
    
    func showMessage(title:String,message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "Tamam", style: .default, handler: nil)
        alert.addAction(okButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func firstShowHideButton(_ sender: UIButton) {
        if iconClick == true {
            passwordText.isSecureTextEntry = false
            showHideButton.setImage(#imageLiteral(resourceName: "icon2"), for: .normal)
            iconClick = false
        } else {
            passwordText.isSecureTextEntry = true
            showHideButton.setImage(#imageLiteral(resourceName: "icon1"), for: .normal)
            iconClick = true
        }
    }
    
    @IBAction func secondShowHideButton(_ sender: UIButton) {
        if secondIconClick == true {
            confirmPasswordText.isSecureTextEntry = false
            showHideClick.setImage(#imageLiteral(resourceName: "icon2"), for: .normal)
            secondIconClick = false
        } else {
            confirmPasswordText.isSecureTextEntry = true
            showHideClick.setImage(#imageLiteral(resourceName: "icon1"), for: .normal)
            secondIconClick = true
        }
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

extension FirstRegisterViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .register }
}
