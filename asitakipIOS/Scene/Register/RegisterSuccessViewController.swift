//
//  RegisterSuccessViewController.swift
//  asitakipIOS
//
//  Created by Mac on 26.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit

class RegisterSuccessViewController: UIViewController {

    @IBOutlet weak var descLbl: UILabel!
    
    var nav: UINavigationController?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension RegisterSuccessViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .register }
}
