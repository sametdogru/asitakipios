//
//  RegisterService.swift
//  asitakipIOS
//
//  Created by Mac on 2.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import SwiftyJSON


struct RegisterUserService {
    
    static func register(_ parameter:[String:Any],vc:BaseVC? = nil,completionHandler: @escaping ( _ respone:CheckResponse?, _ error:MyError?) -> Swift.Void){
           let urlStr = String(format: Service.Register, parameter)
           Network.shared.post(url: urlStr,parameters: parameter, showIndicator:true) {  (response,error) in
               
               var checkRes:CheckResponse?
                
               if error == nil
               {
                checkRes = CheckResponse(json: response)
                if let id = checkRes?.data {
                       print(id)
                       KeychainService.setUserGUID(value: id)
                       KeychainService.setActivationCompleted(value: false)
                   }
               }
           
            if !RegisterUserService.showError(vc, checkRes, error) {
                    completionHandler(checkRes,error)
               }
           }
       }

    static func showError(_ vc:BaseVC?, _ res:CheckResponse?, _ err:MyError?) -> Bool {
           
          guard let vc = vc else { return false }
       
           if let error = err {
            print(error.detail)
            print("error")
               return true
           }
           
           let errors = ErrorCode.errorList()
           
           if let response = res, !response.success
           {
               let arr = errors.filter{$0 == response.code}
               if let _ = arr.first {
                print(response.message)
                let alert = UIAlertController(title: "Uyarı!", message: response.message, preferredStyle: .alert)
                                   let okButton = UIAlertAction(title: "Tamam", style: .cancel, handler: nil)
                                   alert.addAction(okButton)
                vc.present(alert, animated: true, completion: nil)
                   return true
               }
           }
           return false
       }
}


