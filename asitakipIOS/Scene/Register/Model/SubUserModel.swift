//
//  UserModel.swift
//  asitakipIOS
//
//  Created by Mac on 4.05.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//


import Foundation
import SwiftyJSON

//Mark: - User
class SubUsers:NSObject {
    
    @objc dynamic var userGuid:String?
    @objc dynamic var subUserGuid:String?
    @objc dynamic var city:String?
    @objc dynamic var name:String?
    @objc dynamic var surname:String?
    @objc dynamic var gender:String?
    @objc dynamic var birthDate:String?
    dynamic var diseases:[Int]?
    dynamic var profession:Int?
}

extension SubUsers {
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if userGuid != nil{
            dictionary["userGuid"] = userGuid
        }
        if subUserGuid != nil{
            dictionary["subUserGuid"] = subUserGuid
        }
        if name != nil{
            dictionary["name"] = name
        }
        if surname != nil{
            dictionary["surname"] = surname
        }
        if city != nil{
            dictionary["city"] = city
        }
        if gender != nil{
            dictionary["gender"] = gender
        }
        if birthDate != nil{
            dictionary["birthDate"] = birthDate
        }
        if diseases != nil{
            dictionary["diseases"] = diseases
        }
        if profession != nil{
            dictionary["profession"] = profession
        }
        return dictionary
    }
}
