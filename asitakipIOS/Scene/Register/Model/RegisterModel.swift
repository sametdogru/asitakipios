//
//  RegisterModel.swift
//  asitakipIOS
//
//  Created by Mac on 2.04.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import Foundation
import SwiftyJSON

//Mark: - User
class User:NSObject {
    @objc dynamic var email:String?
    @objc dynamic var pwd:String?
    @objc dynamic var city:String?
    @objc dynamic var name:String?
    @objc dynamic var surname:String?
    @objc dynamic var gender:String?
    @objc dynamic var birthDate:String?
    dynamic var profession:Int?
    dynamic var diseases:[Int]?
}

extension User {
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if email != nil{
            dictionary["email"] = email
        }
        if pwd != nil{
            dictionary["password"] = pwd
        }
        if name != nil{
            dictionary["name"] = name
        }
        if surname != nil{
            dictionary["surname"] = surname
        }
        if city != nil{
            dictionary["city"] = city
        }
        if gender != nil{
            dictionary["gender"] = gender
        }
        if birthDate != nil{
            dictionary["birthDate"] = birthDate
        }
        if profession != nil{
            dictionary["profession"] = profession
        }
        if diseases != nil{
            dictionary["diseases"] = diseases
        }
        return dictionary
    }
}
