//
//  SecondRegisterViewController.swift
//  asitakipIOS
//
//  Created by Mac on 26.03.2020.
//  Copyright © 2020 Samet Dogru. All rights reserved.
//

import UIKit
import MBProgressHUD

class SecondRegisterViewController: BaseVC {
    
    @IBOutlet weak var stepView: UIView!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var surnameText: UITextField!
    @IBOutlet weak var professionText: UITextField!
    @IBOutlet weak var diseasesText: UITextField!
    @IBOutlet weak var dateText: UITextField!
    @IBOutlet weak var registerButton: Button!
    @IBOutlet weak var maleRadioButton: UIButton!
    @IBOutlet weak var femaleRadioButton: UIButton!
    
    var isRegister: Bool = false
    var isSelectRadioButton: Bool = false
    var aggrementCheck: Bool = false
    var gender = String()
    var professionIndex = Int()
    var diseasesIndex = [Int]()
    var vaccineDate = String()
    
    var subUserDetails:[Users]?
    var user:User = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Kayıt"
        self.leftNavigationItem = .back
        registerButton.setTitle("Kaydol", for: .normal)
        self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func femaleRadioButton(_ sender: AnyObject) {
        if femaleRadioButton.isSelected == false {
            femaleRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            maleRadioButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
            isSelectRadioButton = true
            gender = "K"
        }
    }
    
    @IBAction func maleRadioButton(_ sender: Any) {
        if maleRadioButton.isSelected == false {
            maleRadioButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            femaleRadioButton.setImage(#imageLiteral(resourceName: "normal"), for: .normal)
            isSelectRadioButton = true
            gender = "E"
        }
    }
    
    @IBAction func dateSelect(_ sender: UITextField) {
        sender.resignFirstResponder()
        let vc = DatePickerController.instantiate()
        vc.delegate = self
        vc.isPerson = true
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func professionSelect(_ sender: UITextField) {
        sender.resignFirstResponder()
        let vc = ProfessionPickerController.instantiate()
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func diseasesSelect(_ sender: UITextField) {
        sender.resignFirstResponder()
        let vc = DiseasesPickerController.instantiate()
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func userAggrementBtn(_ sender: UIButton) {
        let vc = UserAggrementViewController.instantiate()
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func checkBtn(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            self.aggrementCheck = true
        } else {
            self.aggrementCheck = false
        }
    }
    
    @IBAction func registerButton(_ sender: Any) {
        
        if let name = nameText.text, let surname = surnameText.text, let profession = professionText.text, let diseases = diseasesText.text, let date = dateText.text, surname.isEmpty || name.isEmpty || profession.isEmpty || diseases.isEmpty || date.isEmpty || isSelectRadioButton == false || self.aggrementCheck == false {
            self.alert(message: "Lütfen boş alanları doldurunuz..")
        } else {
            user.name = nameText.text
            user.surname = surnameText.text
            user.gender = gender
            user.city = "Ankara"
            user.birthDate = vaccineDate
            user.profession = professionIndex
            user.diseases = diseasesIndex
            registerUser()
        }
    }
}

extension SecondRegisterViewController {
    
    private func registerUser() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        RegisterUserService.register(user.toDictionary() , vc: self) {(checkResp, error) in
            if checkResp != nil
            {
                if !checkResp!.success {
                    print("Error")
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.alert(message: checkResp!.message)
                }else{
                    print(checkResp!.message)
                    print("Success")
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.getUser()
                }
            }
        }
    }
    
    private func getUser() {
        let param = ["userGuid": KeychainService.getUserGUID() ?? ""] as [String : Any]
        UserService.getUser(param, vc: self) { (users, error) in
            self.subUserDetails = users
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = .init(identifier: "tr_TR")
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let year = dateFormatter.date(from: self.vaccineDate)
            let yearComponent = Calendar.current.dateComponents([.year], from: year!, to: Date())
            let yearly = yearComponent.year ?? 0
            
            if yearly > 18 || yearly == 18 {
                let vc = QuestionPageAdultViewController.instantiate()
                vc.subUserGuid = self.subUserDetails?.first?.subUserGuid ?? ""
                vc.isRegister = true
                self.show(vc, sender: nil)
            } else {
                let vc = HomeViewController.instantiate()
                vc.isRegister = true
                self.show(vc, sender: nil)
            }
        }
    }
}

extension SecondRegisterViewController: DatePickerProtocol {
    func selectedDate(date: String) {
        formatter(date: date)
        dateText.text = date
    }
    
    func formatter(date:String) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = .init(identifier: "tr_TR")
        dateFormatter.dateFormat = "dd.MM.yyyy"
        if let vaccinedate = dateFormatter.date(from: date) {
            let vaccineDateFormatter = DateFormatter()
            vaccineDateFormatter.dateFormat = "yyyy-MM-dd"
            self.vaccineDate = vaccineDateFormatter.string(from: vaccinedate)
        }
    }
}

extension SecondRegisterViewController: ProfessionPickerProtocol {
    func selectedProfession(profession: String, index: Int) {
        self.professionText.text = profession
        self.professionIndex = index
    }
}

extension SecondRegisterViewController: DiseasesPickerProtocol {
    func selectedDiseases(diseases: [String], index: [Int]) {
        self.diseasesText.text = diseases.joined(separator: ", ")
        self.diseasesIndex = index
    }
}

extension SecondRegisterViewController: StoryboardInstatiate, Reusable {
    public static var storyboard: Storyboard { return .register }
}
